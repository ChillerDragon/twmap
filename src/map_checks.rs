use crate::map::*;
use crate::compression::DecompressionError;
use crate::convert::{To, TryTo};

use image::RgbaImage;
use ndarray::Array2;
use sanitize_filename::{sanitize_with_options, Options};
use thiserror::Error;

use std::mem;
use std::fmt;

#[derive(Error, Debug)]
#[error("{0}")]
pub enum MapError {
    Vanilla(#[from] VanillaError),
    DDNet(#[from] DDNetError),

    Info(#[from] InfoError),
    Image(#[from] ImageError),
    Envelope(#[from] EnvelopeError),
    Group(#[from] GroupError),
    Layer(#[from] LayerError),
    Sound(#[from] SoundError),
}

#[derive(Error, Debug)]
pub enum DDNetError {
    Bezier,
}

impl fmt::Display for DDNetError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use DDNetError::*;
        match self {
            Bezier => write!(f, "Bezier curves")?,
        }
        write!(f, " are not compatible with DDNet maps")
    }
}

#[derive(Error, Debug)]
pub enum VanillaError {
    InfoSettings,
    DDNetLayer(LayerKind),
    TilesAutoMapper,
    Sounds,
    SoundEnv,
}

impl fmt::Display for VanillaError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use VanillaError::*;
        match self {
            InfoSettings => write!(f, "Settings in the map info")?,
            DDNetLayer(kind) => write!(f, "{:?} layers", kind)?,
            TilesAutoMapper => write!(f, "Auto mappers")?,
            Sounds => write!(f, "Map sounds")?,
            SoundEnv => write!(f, "Sound envelopes")?,
        }
        write!(f, " are not compatible with Vanilla maps")
    }
}

#[derive(Error, Debug)]
pub struct AmountError(usize);

impl fmt::Display for AmountError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Too many ({}), max is {}", self.0, u16::MAX)
    }
}

fn check_amount<T>(items: &[T]) -> Result<(), AmountError> {
    if items.len() > u16::MAX.into() {
        Err(AmountError(items.len()))
    }
    else {
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct SanitizationError(String);

impl fmt::Display for SanitizationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Non-sanitized string: {}, sanitized: {}", self.0, sanitize(&self.0))
    }
}

fn sanitize(string: &String) -> String {
    sanitize_with_options(string, Options {
        windows: true,
        truncate: true,
        replacement: "",
    })
}

// used for image and sound names
fn check_string_sanitization(string: &str, extension: &str) -> Result<(), SanitizationError> {
    let mut filename = string.to_string();
    filename.push('.');
    filename.extend(extension.chars());

    let sanitized = sanitize(&filename);
    if sanitized != filename {
        Err(SanitizationError(filename))
    }
    else {
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct StringLenError {
    string: String,
    max_length: usize,
}

impl fmt::Display for StringLenError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "String '{}' is {} bytes long, max is {}", self.string, self.string.len(), self.max_length)
    }
}

fn check_string_length(string: &str, max_length: usize) -> Result<(), StringLenError> {
    if string.len() > max_length {
        Err(StringLenError {
            string: string.to_string(),
            max_length
        })
    }
    else {
        Ok(())
    }
}

impl TwMap {
    pub fn check(&self) -> Result<(), MapError> {
        self.check_info()?;
        self.check_images()?;
        self.check_envelopes()?;
        self.check_groups()?;
        self.check_layers()?;
        self.check_sounds()?;

        match self.version {
            Version::Teeworlds07 => self.check_vanilla()?,
            Version::DDNet06 => self.check_ddnet()?,
        }

        Ok(())
    }

    pub fn check_ddnet(&self) -> Result<(), DDNetError> {
        use DDNetError::*;
        if bezier_in_use(&self.envelopes) {
            Err(Bezier)?;
        }
        Ok(())
    }

    pub fn check_vanilla(&self) -> Result<(), VanillaError> {
        use VanillaError::*;
        if !self.info.settings.is_empty() {
            Err(InfoSettings)?;
        }
        for group in &self.groups {
            for layer in &group.layers {
                if layer.is_ddnet_layer() {
                    Err(DDNetLayer(layer.kind()))?;
                    if let Layer::Tiles(layer) = layer {
                        if layer.auto_mapper != AutoMapper::default() {
                            Err(TilesAutoMapper)?;
                        }
                    }
                }
            }
        }
        if !self.sounds.is_empty() {
            Err(Sounds)?;
        }
        if self.envelopes.iter().any(|env| env.env_kind() == EnvelopeKind::Sound) {
            Err(SoundEnv)?;
        }
        Ok(())
    }

    pub fn check_individually(&mut self) -> Vec<MapError> {
        let mut errors = Vec::new();
        if let Err(err) = self.check_info() {
            errors.push(err.into());
        }
        if let Err(err) = self.check_images() {
            errors.push(err.into());
        }
        if let Err(err) = self.check_envelopes() {
            errors.push(err.into());
        }
        if let Err(err) = self.check_groups() {
            errors.push(err.into());
        }
        else {
            // check_layers relies on game layer check from check_groups
            if let Err(err) = self.check_layers() {
                errors.push(err.into());
            }
        }
        if let Err(err) = self.check_sounds() {
            errors.push(err.into());
        }
        match self.version {
            Version::Teeworlds07 => if let Err(err) = self.check_vanilla() {
                errors.push(err.into());
            },
            Version::DDNet06 => if let Err(err) = self.check_ddnet() {
                errors.push(err.into());
            },
        }
        errors
    }

    pub fn check_info(&self) -> Result<(), InfoError> {
        self.info.check()
    }

    pub fn check_images(&self) -> Result<(), ImageError> {
        Image::check_all(&self.images)
    }

    pub fn check_envelopes(&self) -> Result<(), EnvelopeError> {
        Envelope::check_all(&self.envelopes)
    }

    pub fn check_groups(&self) -> Result<(), GroupError> {
        Group::check_all(&self.groups)
    }

    pub fn check_layers(&self) -> Result<(), LayerError> {
        let envelope_types = envelope_kinds(&self.envelopes);
        let images = self.images.iter()
            .map(|image| if let Image::Embedded(image) = image {
                match &image.image {
                    CompressedData::Compressed(_, _, info) => info.width % 16 == 0 && info.height % 16 == 0,
                    CompressedData::Loaded(image) => image.width() % 16 == 0 && image.height() % 16 == 0,
                }
            }
            else {
                true
            })
            .collect();
        Layer::check_all(&self.groups, &envelope_types, images, self.sounds.len())
    }

    pub fn check_sounds(&self) -> Result<(), SoundError> {
        Sound::check_all(&self.sounds)
    }
}

pub trait CheckData {
    type ErrorKind: From<Self::CompressedErrorKind>;
    type CompressedErrorKind;

    fn check_loaded_data(&self) -> Result<(), Self::ErrorKind>;

    fn check_compressed_data(&self) -> Result<(), Self::CompressedErrorKind>;

    fn check_data(&self) -> Result<(), Self::ErrorKind> {
        self.check_compressed_data()?;
        self.check_loaded_data()?;
        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum InfoError {
    #[error("Info error: Author - {0}")]
    AuthorLength(StringLenError),
    #[error("Info error: Author - {0}")]
    VersionLength(StringLenError),
    #[error("Info error: Author - {0}")]
    CreditsLength(StringLenError),
    #[error("Info error: Author - {0}")]
    LicenseLength(StringLenError),
    #[error("Info error: Setting {1} - {0}")]
    SettingLength(StringLenError, usize),
}

impl Info {
    fn check(&self) -> Result<(), InfoError> {
        check_string_length(&self.author, Info::MAX_AUTHOR_LENGTH)
            .map_err(|err| InfoError::AuthorLength(err))?;
        check_string_length(&self.version, Info::MAX_VERSION_LENGTH)
            .map_err(|err| InfoError::VersionLength(err))?;
        check_string_length(&self.credits, Info::MAX_CREDITS_LENGTH)
            .map_err(|err| InfoError::CreditsLength(err))?;
        check_string_length(&self.license, Info::MAX_LICENSE_LENGTH)
            .map_err(|err| InfoError::LicenseLength(err))?;

        for (i, setting) in self.settings.iter().enumerate() {
            check_string_length(&setting, Info::MAX_SETTING_LENGTH)
                .map_err(|err| InfoError::SettingLength(err, i))?;
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct ImageError {
    pub index: Option<usize>,
    pub kind: ImageErrorKind,
}

#[derive(Error, Debug)]
pub enum ImageErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameSanitization(#[from] SanitizationError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("{0}")]
    Compressed(#[from] CompressedImageError),
    #[error("Width is too big: {0}, max is {}", i32::MAX)]
    TooBigWidth(u32),
    #[error("Height is too big: {0}, max is {}", i32::MAX)]
    TooBigHeight(u32),
    #[error("Zero width not allowed")]
    ZeroWidth,
    #[error("Zero height not allowed")]
    ZeroHeight,
}

#[derive(Error, Debug)]
pub enum CompressedImageError {
    #[error("Negative width: {0}")]
    NegativeWidth(i32),
    #[error("Negative height: {0}")]
    NegativeHeight(i32),
    #[error("Data size ({0}) is invalid, expected is width {} * height {} * 4 = {}", .1.width, .1.height, .1.width.to::<i64>() * .1.height.to::<i64>() * 4)]
    WrongCompressedDataSize(usize, ImageLoadInfo),
    #[error("zlib decompression failed - {0}")]
    ZLibDecompression(#[from] DecompressionError),
}

impl fmt::Display for ImageError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Image error")?;
        if let Some(index) = self.index {
            write!(f, " at index {} ", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl CheckData for CompressedData<RgbaImage, ImageLoadInfo> {
    type ErrorKind = ImageErrorKind;
    type CompressedErrorKind = CompressedImageError;

    fn check_loaded_data(&self) -> Result<(), ImageErrorKind> {
        use ImageErrorKind::*;
        if let CompressedData::Loaded(image) = self {
            if image.width() > i32::MAX.try_to::<u32>() {
                Err(TooBigWidth(image.width()))?;
            }
            if image.height() > i32::MAX.try_to::<u32>() {
                Err(TooBigHeight(image.height()))?;
            }
            if image.width() == 0 {
                Err(ZeroWidth)?;
            }
            if image.height() == 0 {
                Err(ZeroHeight)?;
            }
        }
        Ok(())
    }

    fn check_compressed_data(&self) -> Result<(), CompressedImageError> {
        use CompressedImageError::*;
        if let CompressedData::Compressed(_, data_size, info) = self {
            if info.width < 0 {
                Err(NegativeWidth(info.width))?;
            }
            if info.height < 0 {
                Err(NegativeHeight(info.height))?;
            }
            let expected_size = info.width.to::<i64>() * info.height.to::<i64>() * 4;
            if (*data_size).try_to::<i64>() != expected_size {
                Err(WrongCompressedDataSize(*data_size, *info))?;
            }
        }
        Ok(())
    }
}

impl Image {
    fn check_all(images: &[Image]) -> Result<(), ImageError> {
        check_amount(images)
            .map_err(|err| ImageError {
                index: None,
                kind: err.into(),
            })?;
        for (i, image) in images.iter().enumerate() {
            image.check()
                .map_err(|kind| ImageError {
                    index: Some(i),
                    kind
                })?;
        }
        Ok(())
    }

    fn check(&self) -> Result<(), ImageErrorKind> {
        check_string_sanitization(self.name(), "png")?;
        check_string_length(self.name(), Image::MAX_NAME_LENGTH)?;

        if let Image::Embedded(image) = self {
            image.image.check_data()?;
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct EnvelopeError {
    pub kind: EnvelopeErrorKind,
    pub index: Option<usize>,
    pub envelope_kind: Option<EnvelopeKind>,
}

#[derive(Error, Debug)]
pub enum EnvelopeErrorKind {
    #[error("Name - {0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("{0}")]
    EnvPoint(#[from] EnvPointError),
}

#[derive(Error, Debug)]
pub struct EnvPointError {
    kind: EnvPointErrorKind,
    index: usize,
}

#[derive(Error, Debug)]
pub enum EnvPointErrorKind {
    #[error("Curve type with id {0} is unknown")]
    UnknownCurveType(i32),
    #[error("Timestamp is at {1} while the one before was at {0}")]
    Ordering(i32, i32)
}

impl fmt::Display for EnvPointError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Faulty envelope point at index {} - {}", self.index, self.kind)
    }
}

impl fmt::Display for EnvelopeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(kind) = self.envelope_kind {
            write!(f, "{:?} ", kind)?;
        }
        write!(f, "envelope error")?;
        if let Some(index) = self.index {
            write!(f, "at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

fn check_env_points<T>(points: &[EnvPoint<T>]) -> Result<(), EnvPointError> {
    use EnvPointErrorKind::*;
    for (i, pair) in points.windows(2).enumerate() {
        if pair[0].time > pair[1].time {
            Err(EnvPointError {
                kind: Ordering(pair[0].time, pair[1].time),
                index: i + 1,
            })?;
        }
    }

    use CurveKind::*;
    for (i, point) in points.iter().enumerate() {
        match point.curve {
            Step | Linear | Fast | Smooth | Slow | Bezier(_) => {},
            Unknown(n) => Err(EnvPointError {
                kind: UnknownCurveType(n),
                index: i,
            })?,
        }
    }
    Ok(())
}

pub fn envelope_kinds(envelopes: &[Envelope]) -> Vec<EnvelopeKind> {
    envelopes.iter()
        .map(|env| env.env_kind())
        .collect()
}

impl Envelope {
    pub fn env_kind(&self) -> EnvelopeKind {
        use EnvelopeKind::*;
        match self {
            Envelope::Position(_) => Position,
            Envelope::Color(_) => Color,
            Envelope::Sound(_) => Sound,
        }
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum EnvelopeKind {
    Position,
    Color,
    Sound,
}

pub(crate) fn bezier_in_use(envelopes: &[Envelope]) -> bool {
    for envelope in envelopes {
        match envelope {
            Envelope::Position(env) => {
                for point in &env.points {
                    match point.curve {
                        CurveKind::Bezier(_) => return true,
                        _ => {},
                    }
                }
            }
            Envelope::Color(env) => {
                for point in &env.points {
                    match point.curve {
                        CurveKind::Bezier(_) => return true,
                        _ => {},
                    }
                }
            }
            Envelope::Sound(env) => {
                for point in &env.points {
                    match point.curve {
                        CurveKind::Bezier(_) => return true,
                        _ => {},
                    }
                }
            }
        }
    }
    false
}

impl Envelope {
    fn check(&self) -> Result<(), EnvelopeErrorKind> {
        match self {
            Envelope::Color(env) => {
                check_env_points(&env.points)?;
            },
            Envelope::Position(env) => {
                check_env_points(&env.points)?;
            },
            Envelope::Sound(env) => {
                check_env_points(&env.points)?;
            }
        }
        check_string_length(self.name(), Envelope::MAX_NAME_LENGTH)?;

        Ok(())
    }

    fn check_all(envelopes: &[Envelope]) -> Result<(), EnvelopeError> {
        check_amount(envelopes)
            .map_err(|err| EnvelopeError {
                kind: err.into(),
                index: None,
                envelope_kind: None
            })?;
        for (i, env) in envelopes.iter().enumerate() {
            env.check()
                .map_err(|kind|
                    EnvelopeError {
                        kind,
                        index: Some(i),
                        envelope_kind: Some(env.env_kind()),
                    })?;
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct GroupError {
    pub index: Option<usize>,
    pub kind: GroupErrorKind
}

#[derive(Error, Debug)]
pub enum GroupErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("The physics group is missing")]
    NoPhysicsGroup,
    #[error("More than one physics group: {0}")]
    MultiplePhysicsGroups(u16),
    #[error("Incorrect values as a physics group")]
    PhysicsGroupValues,
}

impl fmt::Display for GroupError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Group error")?;
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl From<GroupErrorKind> for GroupError {
    fn from(kind: GroupErrorKind) -> Self {
        GroupError {
            index: None,
            kind
        }
    }
}

impl Group {
    fn check(&self) -> Result<(), GroupErrorKind> {
        check_string_length(&self.name, Group::MAX_NAME_LENGTH)?;
        Ok(())
    }

    fn check_all(groups: &[Group]) -> Result<(), GroupError> {
        check_amount(groups)
            .map_err(|err| GroupErrorKind::from(err))?;
        for (i, group) in groups.iter().enumerate() {
            group.check()
                .map_err(|kind|
                    GroupError {
                        index: Some(i),
                        kind
                    })?;
        }
        groups_check_physics_layer(groups)?;
        Ok(())
    }

    // the variables of the physics group should not be edited
    fn verify_physics_group(&self) -> Result<(), GroupErrorKind> {
        use GroupErrorKind::*;
        if self.offset_x != 0
            || self.offset_y != 0
            || self.parallax_x != 100
            || self.parallax_y != 100
            || self.clipping != false
            || self.clip_x != 0
            || self.clip_y != 0
            || self.clip_width != 0
            || self.clip_height != 0
            || self.name != "Game" {
            Err(PhysicsGroupValues)
        }
        else {
            Ok(())
        }
    }
}

// ensure that there one physics group and it having the correct values
fn groups_check_physics_layer(map_groups: &[Group]) -> Result<(), GroupError> {
    use GroupErrorKind::*;
    // ensure exactly one physics group
    let mut physics_group = None;
    let mut physics_group_index = 0;
    let mut physics_group_count = 0;

    for (i, group) in map_groups.iter().enumerate() {
        if group.layers.iter()
            .any(|layer| layer.kind().is_physics_layer()) {
            physics_group = Some(group);
            physics_group_index = i;
            physics_group_count += 1;
        }
    }

    if physics_group_count == 0 {
        Err(NoPhysicsGroup)?;
    }
    if physics_group_count > 1 {
        Err(MultiplePhysicsGroups(physics_group_count))?;
    }

    physics_group.unwrap().verify_physics_group()
        .map_err(|kind| GroupError {
            index: Some(physics_group_index),
            kind
        })?;

    Ok(())
}

#[derive(Error, Debug)]
pub struct OutOfBoundsError(usize);

impl fmt::Display for OutOfBoundsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "is out of bounds, there are only {}", self.0)
    }
}

fn check_bound(index: u16, len: usize) -> Result<(), OutOfBoundsError> {
    if usize::from(index) >= len {
        Err(OutOfBoundsError(len))
    }
    else {
        Ok(())
    }
}

fn check_opt_bound(index: Option<u16>, len: usize) -> Result<(), OutOfBoundsError> {
    if let Some(index) = index {
        if usize::from(index) >= len {
            Err(OutOfBoundsError(len))?;
        }
    }
    Ok(())
}

#[derive(Error, Debug)]
pub struct ReferencedEnvelopeError {
    index: u16,
    env_kind: EnvelopeKind,
    kind: ReferencedEnvelopeErrorKind,
}

impl fmt::Display for ReferencedEnvelopeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Referenced {:?} envelope at index {} {}", self.env_kind, self.index, self.kind)
    }
}

#[derive(Error, Debug)]
pub enum ReferencedEnvelopeErrorKind {
    #[error("{0}")]
    OutOfBounds(#[from] OutOfBoundsError),
    #[error("is actually an {0:?} envelope")]
    WrongKind(EnvelopeKind),
}

fn check_envelope_index(index: Option<u16>, env_kind: EnvelopeKind, env_types: &[EnvelopeKind]) -> Result<(), ReferencedEnvelopeError> {
    check_envelope_index_helper(index, env_kind, env_types)
        .map_err(|kind| ReferencedEnvelopeError {
            index: index.unwrap(),
            env_kind,
            kind,
        })
}

fn check_envelope_index_helper(index: Option<u16>, kind: EnvelopeKind, env_types: &[EnvelopeKind]) -> Result<(), ReferencedEnvelopeErrorKind> {
    if let Some(index) = index {
        check_bound(index, env_types.len())?;
        if env_types[index.to::<usize>()] != kind {
            Err(ReferencedEnvelopeErrorKind::WrongKind(env_types[index.to::<usize>()]))?;
        }
    }
    Ok(())
}

#[derive(Error, Debug)]
pub struct LayerError {
    pub layer_group: Option<(usize, Option<usize>)>,
    pub layer_kind: Option<LayerKind>,
    pub kind: LayerErrorKind,
}

#[derive(Error, Debug)]
pub enum LayerErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("{0}")]
    ReferencedEnvelope(#[from] ReferencedEnvelopeError),
    #[error("Missing")]
    MissingGameLayer,
    #[error("Invalid layer kind")]
    UnknownLayerKind,
    #[error("There is already a physics layer of this kind")]
    DuplicatePhysicsLayer,
    #[error("Different dimensions (height, width): {1:?} than the game layer {0:?}")]
    DifferentPhysicsLayerDimensions((usize, usize), (usize, usize)),
    #[error("Image index {0}")]
    ImageOutOfBounds(OutOfBoundsError),
    #[error("Sound index {0}")]
    SoundOutOfBounds(OutOfBoundsError),
    #[error("Auto mapper seed {0} is invalid, must be positive and less than 1,000,001")]
    AutoMapperSeed(i32),
    #[error("Quad error - {0}")]
    Quad(#[from] QuadError),
    #[error("Sound source error - {0}")]
    SoundSource(#[from] SoundSourceError),
    #[error("Layer is too wide ({0}), max width is {}", i32::MAX)]
    TooBigWidth(usize),
    #[error("Layer is too high ({0}), max height is {}", i32::MAX)]
    TooBigHeight(usize),
    #[error("Layer is not wide enough({0}), min width is 2")]
    TooSmallWidth(usize),
    #[error("Layer is not high enough({0}), min height is 2")]
    TooSmallHeight(usize),
    #[error("Invalid tile - {0}")]
    InvalidTile(#[from] TileError),
    #[error("Image width or height isn't divisible by 16")]
    InvalidImageDimensions,
    #[error("{0}")]
    Compressed(#[from] CompressedLayerError),
}

#[derive(Error, Debug)]
pub enum CompressedLayerError {
    #[error("Negative width: {0}")]
    NegativeWidth(i32),
    #[error("Negative height: {0}")]
    NegativeHeight(i32),
    #[error("Compressed data size {0} is invalid: expected is width {} * height {} * tile size {} = {}", (.1).0, (.1).1, (.1).2, (.1).0.to::<i64>() * (.1).1.to::<i64>() * (.1).2.try_to::<i64>())]
    UncompressedDataSize(usize, (i32, i32, usize)),
    #[error("zlib decompression failed: {0}")]
    ZLibDecompression(DecompressionError),
    #[error("0.7 decompression failed: {0}")]
    VanillaDecompression(DecompressionError),
}

impl fmt::Display for LayerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(kind) = self.layer_kind {
            write!(f, "{:?} ", kind)?;
        }
        write!(f, "layer error")?;
        if let Some((layer_index, group_index)) = self.layer_group {
            write!(f, " at index {}", layer_index)?;
            if let Some(group_index) = group_index {
                write!(f, ", in group at index {}", group_index)?;
            }
        }
        write!(f, ": {}", self.kind)
    }
}

fn check_physics_layer_shapes(groups: &[Group]) -> Result<(), LayerError> {
    // figure out physics layer size, which must be uniform
    let expected_shape = match groups.iter()
        .flat_map(| group| group.layers.iter())
        .find(|layer| layer.kind() == LayerKind::Game) {
        None => Err(LayerError {
            layer_group: None,
            layer_kind: Some(LayerKind::Game),
            kind: LayerErrorKind::MissingGameLayer
        })?,
        Some(layer) => layer.shape().unwrap(),
    };
    for (group_index, group) in groups.iter().enumerate() {
        for (layer_index, layer) in group.layers.iter().enumerate() {
            if layer.kind() == LayerKind::Tiles {
                continue;
            }
            if let Some(shape) = layer.shape() {
                if shape != expected_shape {
                    Err(LayerError {
                        layer_group: Some((layer_index, Some(group_index))),
                        layer_kind: Some(layer.kind()),
                        kind: LayerErrorKind::DifferentPhysicsLayerDimensions(expected_shape, shape),
                    })?;
                }
            }
        }
    }
    Ok(())
}

impl Layer {
    fn is_ddnet_layer(&self) -> bool {
        use Layer::*;
        match self {
            Game(_) | Tiles(_) | Quads(_) => false,
            Front(_) | Tele(_) | Speedup(_) | Switch(_) | Tune(_) => true,
            Sounds(_) => true,
            Invalid(_) => false,
        }
    }

    fn check(&self, mut duplicates: &mut [bool; 6], env_types: &[EnvelopeKind], images: &[bool], sound_count: usize)
             -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        use Layer::*;
        match self {
            Game(layer) => layer.check(&mut duplicates),
            Tiles(layer) => layer.check(env_types, images),
            Quads(layer) => layer.check(env_types, images.len()),
            Front(layer) => layer.check(&mut duplicates),
            Tele(layer) => layer.check(&mut duplicates),
            Speedup(layer) => layer.check(&mut duplicates),
            Switch(layer) => layer.check(&mut duplicates),
            Tune(layer) => layer.check(&mut duplicates),
            Sounds(layer) => layer.check(env_types, sound_count),
            Invalid(_) => Err(UnknownLayerKind.into())
        }
    }

    fn check_all(groups: &[Group], env_types: &[EnvelopeKind], images: Vec<bool>, sound_count: usize) -> Result<(), LayerError> {
        check_amount(&groups.iter().flat_map(|group| group.layers.iter()).collect::<Vec<_>>())
            .map_err(|err| LayerError {
                layer_group: None,
                layer_kind: None,
                kind: err.into(),
            })?;
        let mut duplicates = [false; 6];
        for (group_index, group) in groups.iter().enumerate() {
            for (layer_index, layer) in group.layers.iter().enumerate() {
                layer.check(&mut duplicates, env_types, &images, sound_count)
                    .map_err(|kind| LayerError {
                            layer_group: Some((layer_index, Some(group_index))),
                            layer_kind: Some(layer.kind()),
                            kind
                        })?;
            }
        }
        check_physics_layer_shapes(groups)?;
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct TileError {
    pub pos: (usize, usize),
    pub kind: TileErrorKind,
}

#[derive(Error, Debug)]
pub enum TileErrorKind {
    #[error("Skip byte of tile is {0} instead of zero")]
    TileSkip(u8),
    #[error("Unused byte of tile is {0} instead of zero")]
    TileUnused(u8),
    #[error("Unknown tile flags used, flags: {:#010b}", .0)]
    UnknownTileFlags(u8),
    #[error("Opaque tile flag used in physics layer")]
    OpaqueTileFlag,
    #[error("Unused byte of speedup is {0} instead of zero")]
    SpeedupUnused(u8),
    #[error("Angle of speedup is {0}, but should be between 0 and (exclusive) 360")]
    SpeedupAngle(i16),
}

impl fmt::Display for TileError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Faulty tile at y = {} and x = {} - {}", self.pos.0, self.pos.1, self.kind)
    }
}

pub trait TileChecking {
    fn check(&self) -> Result<(), TileErrorKind> { Ok(()) }
}

impl TileFlags {
    fn check(self) -> bool {
        match TileFlags::from_bits(self.bits()) {
            None => false,
            Some(_) => true,
        }
    }
}

impl TileChecking for Tile {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if self.skip != 0 {
            Err(TileSkip(self.skip))?;
        }
        if self.unused != 0 {
            Err(TileUnused(self.unused))?;
        }
        if !self.flags.check() {
            Err(UnknownTileFlags(self.flags.bits()))?;
        }
        Ok(())
    }
}

impl TileChecking for GameTile {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if self.skip != 0 {
            Err(TileSkip(self.skip))?;
        }
        if self.unused != 0 {
            Err(TileUnused(self.unused))?;
        }
        if !self.flags.check() {
            Err(UnknownTileFlags(self.flags.bits()))?;
        }
        if self.flags.contains(TileFlags::OPAQUE) {
            Err(OpaqueTileFlag)?;
        }
        Ok(())
    }
}

impl TileChecking for Tele {}

impl TileChecking for Switch {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if !self.flags.check() {
            Err(UnknownTileFlags(self.flags.bits()))?;
        }
        if self.flags.contains(TileFlags::OPAQUE) {
            Err(OpaqueTileFlag)?;
        }
        Ok(())
    }
}

impl TileChecking for Speedup {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if self.unused_padding != 0 {
            Err(SpeedupUnused(self.unused_padding))?;
        }
        let angle = i16::from(self.angle);
        if angle < 0 || angle >= 360 {
            Err(SpeedupAngle(angle))?;
        }
        Ok(())
    }
}

impl TileChecking for Tune {}

impl<T: TileChecking> CheckData for CompressedData<Array2<T>, TilesLoadInfo> {
    type ErrorKind = LayerErrorKind;
    type CompressedErrorKind = CompressedLayerError;

    fn check_loaded_data(&self) -> Result<(), Self::ErrorKind> {
        use LayerErrorKind::*;

        if let CompressedData::Loaded(tiles) = self {
            if tiles.ncols() > i32::MAX.try_to::<usize>() {
                Err(TooBigWidth(tiles.ncols()))?;
            }
            if tiles.nrows() > i32::MAX.try_to::<usize>() {
                Err(TooBigHeight(tiles.nrows()))?;
            }
            if tiles.ncols() < 2 {
                Err(TooSmallWidth(tiles.ncols()))?;
            }
            if tiles.nrows() < 2 {
                Err(TooSmallHeight(tiles.nrows()))?;
            }

            match tiles.indexed_iter()
                .map(|(pos, tile)|
                    tile.check()
                        .map_err(|kind|
                            TileError {
                                pos,
                                kind,
                            }))
                .find(|result| result.is_err()) {
                Some(err) => err?,
                None => {},
            }
        }
        Ok(())
    }

    fn check_compressed_data(&self) -> Result<(), Self::CompressedErrorKind> {
        use CompressedLayerError::*;

        if let CompressedData::Compressed(_, size, info) = self {
            if info.width < 0 {
                Err(NegativeWidth(info.width))?;
            }
            if info.height < 0 {
                Err(NegativeHeight(info.height))?;
            }
            if info.compression { // 0.7 compression
                if size % 4 != 0 {
                    Err(VanillaDecompression(DecompressionError::Failed))?;
                }
            }
            else {
                let tile_count = i64::from(info.width) * i64::from(info.height);
                let expected_size = tile_count * mem::size_of::<T>().try_to::<i64>();
                if expected_size != (*size).try_to::<i64>() {
                    Err(UncompressedDataSize(*size, (info.width, info.height, mem::size_of::<T>())))?;
                }
            }
        }
        Ok(())
    }
}

pub trait PhysicsLayerChecking: TileMapLayer
    where Self::TileType: TileChecking {
    fn check(&self, duplicates: &mut [bool; 6]) -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        self.tiles().check_data()?;

        if duplicates[Self::index()] {
            return Err(DuplicatePhysicsLayer)?;
        }
        duplicates[Self::index()] = true;
        Ok(())
    }

    // giving each physics layer an index to check for duplicates
    fn index() -> usize;
}

impl PhysicsLayerChecking for GameLayer {
    fn index() -> usize { 0 }
}
impl PhysicsLayerChecking for FrontLayer {
    fn index() -> usize { 1 }
}
impl PhysicsLayerChecking for TeleLayer {
    fn index() -> usize { 2 }
}
impl PhysicsLayerChecking for SpeedupLayer {
    fn index() -> usize { 3 }
}
impl PhysicsLayerChecking for SwitchLayer {
    fn index() -> usize { 4 }
}
impl PhysicsLayerChecking for TuneLayer {
    fn index() -> usize { 5 }
}

impl AutoMapper {
    fn check(&self) -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        if self.seed < 0 || self.seed > 1_000_000_000 {
            Err(AutoMapperSeed(self.seed))?;
        }
        Ok(())
    }
}

impl TilesLayer {
    fn check(&self, env_types: &[EnvelopeKind], images: &[bool]) -> Result<(), LayerErrorKind> {
        check_envelope_index(self.color_env, EnvelopeKind::Color, env_types)?;
        check_opt_bound(self.image, images.len())
            .map_err(|err| LayerErrorKind::ImageOutOfBounds(err))?;
        if let Some(index) = self.image {
            if !images[index.to::<usize>()] {
                Err(LayerErrorKind::InvalidImageDimensions)?;
            }
        }
        self.tiles.check_data()?;
        self.auto_mapper.check()?;
        check_string_length(&self.name, Layer::MAX_NAME_LENGTH)?;

        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct QuadError {
    index: usize,
    position: Point,
    err: ReferencedEnvelopeError,
}

impl fmt::Display for QuadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Faulty quad at index {} and position {} - {}", self.index, self.position, self.err)
    }
}

impl Quad {
    pub fn check(&self, env_types: &[EnvelopeKind]) -> Result<(), ReferencedEnvelopeError> {
        check_envelope_index(self.position_env, EnvelopeKind::Position, env_types)?;
        check_envelope_index(self.position_env, EnvelopeKind::Position, env_types)?;
        Ok(())
    }
}

impl QuadsLayer {
    fn check(&self, env_types: &[EnvelopeKind], img_count: usize) -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        for (i, quad) in self.quads.iter().enumerate() {
            quad.check(env_types)
                .map_err(|err| QuadError {
                    index: i,
                    position: quad.position,
                    err,
                })?;
        }

        check_opt_bound(self.image, img_count)
            .map_err(|err| ImageOutOfBounds(err))?;
        check_string_length(&self.name, Layer::MAX_NAME_LENGTH)?;

        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum SoundSourceErrorKind {
    #[error("{0}")]
    ReferencedEnvelope(#[from] ReferencedEnvelopeError),
    #[error("delay is negative: {0}")]
    Delay(i32),
    #[error("width is negative: {0}")]
    Width(i32),
    #[error("height is negative: {0}")]
    Height(i32),
    #[error("radius is negative: {0}")]
    Radius(i32),
}

#[derive(Error, Debug)]
pub struct SoundSourceError {
    index: usize,
    position: Point,
    err: SoundSourceErrorKind,
}

impl fmt::Display for SoundSourceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Faulty sound source at index {} and position {} - {}", self.index, self.position, self.err)
    }
}

impl SoundSource {
    fn check(&self, env_types: &[EnvelopeKind]) -> Result<(), SoundSourceErrorKind> {
        use SoundSourceErrorKind::*;
        if self.delay < 0 {
            Err(Delay(self.delay))?;
        }
        check_envelope_index(self.position_env, EnvelopeKind::Position, env_types)?;
        check_envelope_index(self.sound_env, EnvelopeKind::Sound, env_types)?;

        match self.shape {
            SoundShape::Rectangle { width, height } => {
                if width < 0 {
                    Err(Width(width))?;
                }
                if height < 0 {
                    Err(Height(height))?;
                }
            },
            SoundShape::Circle { radius } => {
                if radius < 0 {
                    Err(Radius(radius))?;
                }
            },
        }

        Ok(())
    }
}

impl SoundsLayer {
    fn check(&self, env_types: &[EnvelopeKind], sound_count: usize) -> Result<(), LayerErrorKind> {
        for (i, source) in self.sources.iter().enumerate() {
            source.check(env_types)
                .map_err(|err| SoundSourceError {
                    index: i,
                    position: source.position,
                    err
                })?;
        }

        check_opt_bound(self.sound, sound_count)
            .map_err(|err| LayerErrorKind::SoundOutOfBounds(err))?;
        check_string_length(&self.name, Layer::MAX_NAME_LENGTH)?;
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct SoundError {
    pub index: Option<usize>,
    pub kind: SoundErrorKind
}

#[derive(Error, Debug)]
pub enum SoundErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("Name - {0}")]
    NameSanitization(#[from] SanitizationError),
    #[error("{0}")]
    Compressed(#[from] CompressedSoundError),
    #[error("Invalid sound data: {0}")]
    Opus(#[from] opus_headers::ParseError),
}

#[derive(Error, Debug)]
pub enum CompressedSoundError {
    #[error("zlib decompression failed: {0}")]
    ZLibDecompression(#[from] DecompressionError),
}

impl fmt::Display for SoundError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Sound error")?;
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl CheckData for CompressedData<Vec<u8>, ()> {
    type ErrorKind = SoundErrorKind;
    type CompressedErrorKind = CompressedSoundError;

    fn check_loaded_data(&self) -> Result<(), SoundErrorKind> {
        if let CompressedData::Loaded(buf) = self {
            opus_headers::parse_from_read(&buf[..])?;
        }
        Ok(())
    }

    fn check_compressed_data(&self) -> Result<(), CompressedSoundError> {
        Ok(())
    }
}

impl Sound {
    fn check(&self) -> Result<(), SoundErrorKind> {
        check_string_sanitization(&self.name, "opus")?;
        check_string_length(&self.name, Sound::MAX_NAME_LENGTH)?;

        self.data.check_data()?;
        Ok(())
    }

    fn check_all(sounds: &[Sound]) -> Result<(), SoundError> {
        check_amount(sounds)
            .map_err(|err| SoundError {
                index: None,
                kind: err.into(),
            })?;
        for (i, sound) in sounds.iter().enumerate() {
            sound.check()
                .map_err(|kind| SoundError {
                    index: Some(i),
                    kind,
                })?;
        }
        Ok(())
    }
}
