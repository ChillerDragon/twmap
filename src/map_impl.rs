use crate::map::*;
use crate::convert::TryTo;

use image::RgbaImage;
use ndarray::Array2;

use std::fmt;

impl Info {
    pub const MAX_AUTHOR_LENGTH: usize = 31;
    pub const MAX_VERSION_LENGTH: usize = 15;
    pub const MAX_CREDITS_LENGTH: usize = 127;
    pub const MAX_LICENSE_LENGTH: usize = 31;
    pub const MAX_SETTING_LENGTH: usize = 255;
}

impl Image {
    pub const MAX_NAME_LENGTH: usize = 127;
}

impl Envelope {
    pub const MAX_NAME_LENGTH: usize = 31;
}

impl Group {
    pub const MAX_NAME_LENGTH: usize = 11;
}

impl Layer {
    pub const MAX_NAME_LENGTH: usize = 11;
}

impl Sound {
    pub const MAX_NAME_LENGTH: usize = 127;
}

impl TwMap {
    /// Returns a empty map struct with only the version set.
    pub fn empty(version: Version) -> TwMap {
        TwMap {
            version,
            info: Info::default(),
            images: vec![],
            envelopes: vec![],
            groups: vec![],
            sounds: vec![],
        }
    }

    /// Returns a reference to the physics group.
    pub fn physics_group(&self) -> &Group {
        self.groups.iter()
            .find(|group| group.is_physics_group())
            .unwrap()
    }

    /// Returns a mutable reference to the physics group.
    pub fn physics_group_mut(&mut self) -> &mut Group {
        self.groups.iter_mut()
            .find(|group| group.is_physics_group())
            .unwrap()
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{x: {}, y: {}}}", self.x, self.y)
    }
}

impl Group {
    /// Checks if the group contains any physics layers.
    pub fn is_physics_group(&self) -> bool {
        self.layers.iter()
            .any(|layer| layer.kind().is_physics_layer())
    }
}

impl From<ExternalImage> for Image {
    fn from(image: ExternalImage) -> Self {
        Image::External(image)
    }
}

impl From<EmbeddedImage> for Image {
    fn from(image: EmbeddedImage) -> Self {
        Image::Embedded(image)
    }
}

impl Image {
    pub fn name(&self) -> &String {
        match self {
            Image::External(img) => &img.name,
            Image::Embedded(img) => &img.name,
        }
    }

    pub fn name_mut(&mut self) -> &mut String {
        match self {
            Image::External(img) => &mut img.name,
            Image::Embedded(img) => &mut img.name,
        }
    }

    pub fn width(&self) -> i32 {
        match self {
            Image::External(image) => image.width.try_to(),
            Image::Embedded(image) => match &image.image {
                CompressedData::Compressed(_, _, info) => info.width,
                CompressedData::Loaded(image) => image.width().try_to(),
            },
        }
    }

    pub fn height(&self) -> i32 {
        match self {
            Image::External(image) => image.height,
            Image::Embedded(image) => match &image.image {
                CompressedData::Compressed(_, _, info) => info.height,
                CompressedData::Loaded(image) => image.height().try_to(),
            },
        }
    }

    pub fn image(&self) -> Option<&CompressedData<RgbaImage, ImageLoadInfo>> {
        match self {
            Image::External(_) => None,
            Image::Embedded(image) => Some(&image.image)
        }
    }

    pub fn image_mut(&mut self) -> Option<&mut CompressedData<RgbaImage, ImageLoadInfo>> {
        match self {
            Image::External(_) => None,
            Image::Embedded(image) => Some(&mut image.image)
        }
    }
}

impl LayerKind {
    pub fn is_physics_layer(&self) -> bool {
        use LayerKind::*;
        match self {
            Game | Front | Tele | Speedup | Switch | Tune => true,
            Tiles | Quads | Sounds | Invalid(_) => false,
        }
    }

    pub fn is_tile_map_layer(&self) -> bool {
        use LayerKind::*;
        match self {
            Game | Tiles | Front | Tele | Speedup | Switch | Tune => true,
            Quads | Sounds | Invalid(_) => false,
        }
    }
}

impl<T> CompressedData<Array2<T>, TilesLoadInfo> {
    /// Returns the (height, width) 2-dimensional array of tiles.
    pub fn shape(&self) -> (usize, usize) {
        match self {
            CompressedData::Compressed(_, _, info) => (info.height.try_to(), info.width.try_to::<usize>()),
            CompressedData::Loaded(array) => (array.nrows(), array.ncols()),
        }
    }
}

impl Layer {
    /// Returns an identifier for the type of the layer.
    pub fn kind(&self) -> LayerKind {
        use Layer::*;
        match self {
            Game(_) => LayerKind::Game,
            Tiles(_) => LayerKind::Tiles,
            Quads(_) => LayerKind::Quads,
            Front(_) => LayerKind::Front,
            Tele(_) => LayerKind::Tele,
            Speedup(_) => LayerKind::Speedup,
            Switch(_) => LayerKind::Switch,
            Tune(_) => LayerKind::Tune,
            Sounds(_) => LayerKind::Sounds,
            Invalid(kind) => LayerKind::Invalid(*kind),
        }
    }

    /// Returns the (height, width) of the 2-dimensional array of tiles, if the contained layer is a tile map layer.
    pub fn shape(&self) -> Option<(usize, usize)> {
        use Layer::*;
        match self {
            Game(l) => Some(l.tiles().shape()),
            Tiles(l) => Some(l.tiles().shape()),
            Quads(_) => None,
            Front(l) => Some(l.tiles().shape()),
            Tele(l) => Some(l.tiles().shape()),
            Speedup(l) => Some(l.tiles().shape()),
            Switch(l) => Some(l.tiles().shape()),
            Tune(l) => Some(l.tiles().shape()),
            Sounds(_) => None,
            Invalid(_) => None,
        }
    }
}

impl Quad {
    /// Creates a quad with the center at (0, 0)
    /// Except for the size, it uses the default values of a newly created quad in the editor
    pub fn new(width: i32, height: i32) -> Self {
        Quad {
            corners: [
                Point { x: -(width / 2), y: -(height / 2) },
                Point { x: width / 2, y: -(height / 2) },
                Point { x: -(width / 2), y: height / 2 },
                Point { x: width / 2, y: height / 2 },
            ],
            position: Point { x: 0, y: 0 },
            colors: <[Color; 4]>::default(),
            texture_coords: [
                Point { x: 0, y: 0 },
                Point { x: 1024, y: 0 },
                Point { x: 0, y: 1024 },
                Point { x: 1024, y: 1024 },
            ],
            position_env: None,
            position_env_offset: 0,
            color_env: None,
            color_env_offset: 0
        }
    }
}

impl Default for Quad {
    /// Default settings of a newly created sound source in the editor
    fn default() -> Self {
        Quad::new(1024 * 32 * 2, 1024 * 32 * 2)
    }
}

impl Default for SoundSource {
    /// Default settings of a newly created sound source in the editor
    fn default() -> Self {
        SoundSource {
            position: Point { x: 0, y: 0 },
            looping: true,
            panning: true,
            delay: 0,
            falloff: 80,
            position_env: None,
            position_env_offset: 0,
            sound_env: None,
            sound_env_offset: 0,
            shape: SoundShape::Circle { radius: 1500 },
        }
    }
}

impl<T, U> From<T> for CompressedData<T, U> {
    fn from(data: T) -> Self {
        CompressedData::Loaded(data)
    }
}

impl<T, U> CompressedData<T, U> {
    /// Returns a reference to the inner loaded value. Panics if isn't loaded.
    pub fn unwrap_ref(&self) -> &T {
        match self {
            CompressedData::Compressed(_, _, _) => panic!("Data is still compressed, reference unwrap unsuccessful"),
            CompressedData::Loaded(data) => data,
        }
    }

    /// Returns a mutable reference to the inner loaded value. Panics if isn't loaded.
    pub fn unwrap_mut(&mut self) -> &mut T {
        match self {
            CompressedData::Compressed(_, _, _) => panic!("Data is still compressed, mut reference unwrap unsuccessful"),
            CompressedData::Loaded(data) => data,
        }
    }

    /// Returns the inner loaded value. Panics if isn't loaded.
    pub fn unwrap(self) -> T {
        match self {
            CompressedData::Compressed(_, _, _) => panic!("Data is still compressed, unwrap unsuccessful"),
            CompressedData::Loaded(data) => data,
        }
    }
}

impl Envelope {
    /// Returns a reference to the name of the envelope.
    pub fn name(&self) -> &String {
        use Envelope::*;
        match self {
            Position(env) => &env.name,
            Color(env) => &env.name,
            Sound(env) => &env.name,
        }
    }

    /// Returns a mutable reference to the name of the envelope.
    pub fn name_mut (&mut self) -> &mut String {
        use Envelope::*;
        match self {
            Position(env) => &mut env.name,
            Color(env) => &mut env.name,
            Sound(env) => &mut env.name,
        }
    }
}

impl<T> Default for CurveKind<T> {
    fn default() -> Self {
        CurveKind::Linear
    }
}

impl Default for Group {
    fn default() -> Self {
        Group {
            offset_x: 0,
            offset_y: 0,
            parallax_x: 100,
            parallax_y: 100,
            layers: vec![],
            clipping: false,
            clip_x: 0,
            clip_y: 0,
            clip_width: 0,
            clip_height: 0,
            name: String::new(),
        }
    }
}

impl Group {
    /// Constructor for the game group, make sure not to change any values apart from the layers
    pub fn game() -> Self {
        let mut group = Group::default();
        group.name = String::from("Game");
        group
    }
}

impl TilesLayer {
    /// Creates a tiles layer with the default values and the specified dimensions.
    pub fn new(shape: (usize, usize)) -> Self {
        TilesLayer {
            detail: false,
            color: Color::default(),
            color_env: None,
            color_env_offset: 0,
            image: None,
            tiles: Array2::default(shape).into(),
            name: "".to_string(),
            auto_mapper: AutoMapper::default()
        }
    }
}

impl Layer {
    /// Returns a reference to the name of the layer.
    pub fn name(&self) -> &str {
        use Layer::*;
        match self {
            Game(_) => LayerKind::Game.static_name(),
            Tiles(l) => &l.name,
            Quads(l) => &l.name,
            Front(_) => LayerKind::Front.static_name(),
            Tele(_) => LayerKind::Tele.static_name(),
            Speedup(_) => LayerKind::Speedup.static_name(),
            Switch(_) => LayerKind::Switch.static_name(),
            Tune(_) => LayerKind::Tune.static_name(),
            Sounds(l) => &l.name,
            Invalid(_) => panic!(),
        }
    }

    /// Returns a mutable reference to the name of the layer, if the layer type supports an editable name.
    pub fn name_mut(&mut self) -> Option<&mut String> {
        use Layer::*;
        match self {
            Game(_) => None,
            Tiles(l) => Some(&mut l.name),
            Quads(l) => Some(&mut l.name),
            Front(_) => None,
            Tele(_) => None,
            Speedup(_) => None,
            Switch(_) => None,
            Tune(_) => None,
            Sounds(l) => Some(&mut l.name),
            Invalid(_) => panic!(),
        }
    }
}

impl Tile {
    /// Constructor, required due to unused bytes which will be set to 0.
    pub fn new(id: u8, flags: TileFlags) -> Self {
        Tile {
            id,
            flags,
            skip: 0,
            unused: 0,
        }
    }
}

impl GameTile {
    /// Constructor, required due to unused bytes which will be set to 0.
    pub fn new(id: u8, flags: TileFlags) -> Self {
        GameTile {
            id,
            flags,
            skip: 0,
            unused: 0,
        }
    }
}

impl Speedup {
    /// Constructor, required due to unused bytes which will be set to 0.
    pub fn new(id: u8, force: u8, max_speed: u8, angle: i16) -> Self {
        Speedup {
            force,
            max_speed,
            id,
            unused_padding: 0,
            angle: angle.into(),
        }
    }
}

impl From<i16> for I16 {
    fn from(x: i16) -> Self {
        I16 {
            bytes: x.to_le_bytes(),
        }
    }
}

impl From<I16> for i16 {
    fn from(x: I16) -> Self {
        i16::from_le_bytes(x.bytes)
    }
}

impl Default for Color {
    fn default() -> Self {
        Color {
            r: 255,
            g: 255,
            b: 255,
            a: 255,
        }
    }
}

impl Default for I32Color {
    fn default() -> Self {
        I32Color {
            r: 1024,
            g: 1024,
            b: 1024,
            a: 1024,
        }
    }
}

impl BezierDefault for I32Color {
    fn bezier_default() -> Self {
        I32Color {
            r: 0,
            g: 0,
            b: 0,
            a: 0,
        }
    }
}

impl Default for Position {
    fn default() -> Self {
        Position {
            x: 0,
            y: 0,
            rotation: 0
        }
    }
}

impl BezierDefault for Position {}

impl BezierDefault for i32 {}

impl TileMapLayer for TilesLayer {
    type TileType = Tile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TileMapLayer for GameLayer {
    type TileType = GameTile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TileMapLayer for FrontLayer {
    type TileType = GameTile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TileMapLayer for TeleLayer {
    type TileType = Tele;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TileMapLayer for SpeedupLayer {
    type TileType = Speedup;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TileMapLayer for SwitchLayer {
    type TileType = Switch;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TileMapLayer for TuneLayer {
    type TileType = Tune;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl AnyTile for Tile {
    fn id(&self) -> u8 { self.id }

    fn flags(&self) -> Option<TileFlags> { Some(self.flags) }
}

impl AnyTile for GameTile {
    fn id(&self) -> u8 { self.id }

    fn flags(&self) -> Option<TileFlags> { Some(self.flags) }
}

impl AnyTile for Switch {
    fn id(&self) -> u8 { self.id }

    fn flags(&self) -> Option<TileFlags> { Some(self.flags) }
}

impl AnyTile for Tele {
    fn id(&self) -> u8 { self.id }

    fn flags(&self) -> Option<TileFlags> { None }
}

impl AnyTile for Speedup {
    fn id(&self) -> u8 { self.id }

    fn flags(&self) -> Option<TileFlags> { None }
}

impl AnyTile for Tune {
    fn id(&self) -> u8 { self.id }

    fn flags(&self) -> Option<TileFlags> { None }
}

impl AnyLayer for GameLayer {
    fn kind() -> LayerKind {
        LayerKind::Game
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Game(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Game(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for TilesLayer {
    fn kind() -> LayerKind {
        LayerKind::Tiles
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Tiles(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Tiles(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for QuadsLayer {
    fn kind() -> LayerKind {
        LayerKind::Quads
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Quads(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Quads(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for FrontLayer {
    fn kind() -> LayerKind {
        LayerKind::Front
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Front(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Front(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for TeleLayer {
    fn kind() -> LayerKind {
        LayerKind::Tele
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Tele(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Tele(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for SpeedupLayer {
    fn kind() -> LayerKind {
        LayerKind::Speedup
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Speedup(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Speedup(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for SwitchLayer {
    fn kind() -> LayerKind {
        LayerKind::Switch
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Switch(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Switch(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for TuneLayer {
    fn kind() -> LayerKind {
        LayerKind::Tune
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Tune(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Tune(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}

impl AnyLayer for SoundsLayer {
    fn kind() -> LayerKind {
        LayerKind::Sounds
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Sounds(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Sounds(l) = layer {
            Some(l)
        }
        else {
            None
        }
    }
}
