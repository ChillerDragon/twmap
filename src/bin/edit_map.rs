use twmap::TwMap;

use clap::{App, Arg};

use std::path::PathBuf;
use std::process::exit;

fn main() {
    let matches = App::new("twmap edit_map")
        .about("Loads teeworlds/ddnet maps, applies changes configured through the arguments, saves them again.")
        .arg(Arg::with_name("INPUT-FILE")
            .help("Sets input map file path")
            .required(true)
            .index(1))
        .arg(Arg::with_name("OUTPUT-FILE")
            .help("Sets output file path")
            .required(true)
            .index(2))
        .arg(Arg::with_name("MapDir")
            .help("Save as a directory map.")
            .long("mapdir"))
        .arg(Arg::with_name("mirror")
            .long("mirror")
            .short("m")
            .help("Mirror the map (switches left and right), applies before rotating"))
        .arg(Arg::with_name("rotate-right")
            .long("rotate")
            .short("r")
            .help("Rotate the map clockwise, this flag can be used multiple times")
            .multiple(true))
        .arg(Arg::with_name("remove-everything-unused")
            .long("remove-everything-unused")
            .help("Removes all unused layers, groups, envelopes, images and sounds"))
        .arg(Arg::with_name("remove-unused-envelopes")
            .long("remove-unused-envelopes")
            .help("Removes all unused envelopes"))
        .arg(Arg::with_name("remove-unused-files")
            .long("remove-unused-files")
            .help("Removes all unused images and sounds"))
        .arg(Arg::with_name("extend-up")
            .takes_value(true)
            .long("extend-up")
            .help("Extends all layers upwards by the specified tile amount"))
        .arg(Arg::with_name("extend-left")
            .takes_value(true)
            .long("extend-left")
            .help("Extends all layers left by the specified tile amount"))
        .arg(Arg::with_name("extend-down")
            .takes_value(true)
            .long("extend-down")
            .help("Extends all layers down by the specified tile amount"))
        .arg(Arg::with_name("extend-right")
            .takes_value(true)
            .long("extend-right")
            .help("Extends all layers right by the specified tile amount"))
        .arg(Arg::with_name("shrink-layers")
            .long("shrink-layers")
            .help("Shrinks all layers as much as possible without changing anything. Note that this might move the world border"))
        .arg(Arg::with_name("shrink-tiles-layers")
            .long("shrink-tiles-layers")
            .help("Shrinks all tiles layers as much as possible without changing anything."))
        .get_matches();

    let input_path = PathBuf::from(matches.value_of_os("INPUT-FILE").unwrap());
    let output_path = PathBuf::from(matches.value_of_os("OUTPUT-FILE").unwrap());

    let mut map = match TwMap::parse_path(&input_path) {
        Err(err) => {
            println!("parse error: {}", err);
            exit(1);
        },
        Ok(map) => map,
    };

    if let Err(err) = map.load() {
        println!("error on decompression: {}", err);
        exit(1);
    }

    let mut up_amount = 0;
    if matches.is_present("extend-up") {
        up_amount = match matches.value_of("extend-up").unwrap().parse::<u16>() {
            Ok(up) => up,
            Err(err) => {
                println!("Upwards tile amount couldn't be parsed, aborting: {}", err);
                exit(-1);
            }
        };
    }
    let mut left_amount = 0;
    if matches.is_present("extend-left") {
        left_amount = match matches.value_of("extend-left").unwrap().parse::<u16>() {
            Ok(left) => left,
            Err(err) => {
                println!("Left tile amount couldn't be parsed, aborting: {}", err);
                exit(-1);
            }
        };
    }
    let mut down_amount = 0;
    if matches.is_present("extend-down") {
        down_amount = match matches.value_of("extend-down").unwrap().parse::<u16>() {
            Ok(down) => down,
            Err(err) => {
                println!("Down tile amount couldn't be parsed, aborting: {}", err);
                exit(-1);
            }
        };
    }
    let mut right_amount = 0;
    if matches.is_present("extend-right") {
        right_amount = match matches.value_of("extend-right").unwrap().parse::<u16>() {
            Ok(right) => right,
            Err(err) => {
                println!("Right tile amount couldn't be parsed, aborting: {}", err);
                exit(-1);
            }
        };
    }
    if up_amount != 0 || left_amount != 0 || down_amount!= 0 || right_amount != 0 {
        map = match map.extend_layers(up_amount, down_amount, left_amount, right_amount) {
            None => {
                println!("An overflow occurred while extending the map.");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if matches.is_present("mirror") {
        println!("Mirroring the map!");
        map = match map.mirror() {
            None => {
                println!("An overflow occurred while mirroring the map.");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if matches.is_present("rotate-right") {
        for i in 0..matches.occurrences_of("rotate-right") {
            match i {
                0 => println!("Rotating the map!"),
                _ => println!("Rotating the map! (x{})", i + 1),
            };
            map = match map.rotate_right() {
                None => {
                    println!("An overflow occurred while rotating the map.");
                    exit(-1);
                }
                Some(map) => map,
            }
        }
    }

    if matches.is_present("remove-everything-unused") {
        let removed_layers = map.remove_unused_layers();
        if removed_layers > 0 {
            println!("Removed {} empty layers!", removed_layers);
        }

        let removed_groups = map.remove_unused_layers();
        if removed_groups > 0 {
            println!("Removed {} empty groups!", removed_groups);
        }
    }

    if matches.is_present("remove-unused-envelopes") || matches.is_present("remove-everything-unused") {
        let removed_envelopes = map.remove_unused_envelopes();
        if removed_envelopes > 0 {
            println!("Removed {} unused envelopes!", removed_envelopes);
        }
    }

    if matches.is_present("remove-unused-files") || matches.is_present("remove-everything-unused")  {
        let (removed_external, removed_embedded) = map.remove_unused_images();
        if removed_external > 0 {
            println!("Removed {} unused external images!", removed_external);
        }
        if removed_embedded > 0 {
            println!("Removed {} unused embedded images!", removed_embedded);
        }

        let removed_sounds = map.remove_unused_sounds();
        if removed_sounds > 0 {
            println!("Removed {} unused sounds!", removed_sounds);
        }
    }

    if matches.is_present("shrink-layers") {
        map = match map.lossless_shrink_layers() {
            None => {
                println!("Overflow while shrinking layers");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if matches.is_present("shrink-tiles-layers") {
        map = match map.lossless_shrink_tiles_layers() {
            None => {
                println!("Overflow while shrinking tiles layers");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    // (try to) save map
    if let Err(err) = match matches.is_present("MapDir") {
        true => map.save_dir(&output_path),
        false => map.save_file(&output_path),
    } {
        println!("Failed to save map: {}", err);
        exit(1);
    }
}
