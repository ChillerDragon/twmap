use twmap::{TwMap, Image, Sound, LoadMultiple};

use clap::{App, Arg, ArgGroup};
use sha2::{Sha256, Digest};

use std::error::Error;
use std::ffi::OsString;
use std::fs;
use std::io::Write;

fn main() {
    let matches = App::new("twmap extract_files")
        .about("Extracts images and sounds from teeworlds maps. Saves them to the current working directory.")
        .arg(Arg::with_name("files")
            .required(true)
            .min_values(1)
            .index(1))
        .arg(Arg::with_name("images-only")
            .long("images")
            .short("i")
            .help("Only extract image files (.png)"))
        .arg(Arg::with_name("sounds-only")
            .long("sounds")
            .short("s")
            .help("Only extract sound files (.opus)"))
        .group(ArgGroup::with_name("specify")
            .args(&["images-only", "sounds-only"]))
        .arg(Arg::with_name("omit-hash")
            .long("omit-hash")
            .short("x")
            .help("Omit sha256 postfix -> allow collisions in file names"))
        .get_matches();

    let shorten = matches.is_present("omit-hash");
    let files = matches.values_of_os("files").unwrap()
        .map(|s| s.to_os_string())
        .collect::<Vec<OsString>>();

    for file_path in files.iter() {
        let map = match TwMap::parse_file(file_path) {
            Ok(map) => map,
            Err(err) => {
                eprintln!("{:?}: {}", file_path, err);
                continue;
            }
        };
        println!("Map: {:?}", file_path);
        if !matches.is_present("sounds-only") {
            match save_images(map.images, shorten) {
                Ok(_) => {},
                Err(error) => eprintln!("{:?}: {}", file_path, error),
            }
        }
        if !matches.is_present("images-only") {
            match save_sounds(map.sounds, shorten) {
                Ok(_) => {},
                Err(error) => eprintln!("{:?}: {}", file_path, error),
            }
        }
    }
}

fn save_images(mut images: Vec<Image>, shorten: bool) -> Result<(), Box<dyn Error>> {
    images.load()?;

    for image in images {
        if let Image::Embedded(image) = image {
            let path = match shorten {
                false => format!("{}_{}.png", &image.name, sha256(image.image.unwrap_ref().as_ref())),
                true => format!("{}.png", &image.name),
            };
            image.image.unwrap().save(&path)?;
            println!("\t{}", &path);
        }
    }
    Ok(())
}

fn save_sounds(mut sounds: Vec<Sound>, shorten: bool) -> Result<(), Box<dyn Error>> {
    sounds.load()?;

    for sound in sounds {
        let data = sound.data.unwrap();
        let path = match shorten {
            false => format!("{}_{}.opus", &sound.name, sha256(&data)),
            true => format!("{}.opus", &sound.name),
        };
        let mut file = fs::File::create(&path)?;
        file.write_all(&data[..])?;
        println!("\t{}", &path);
    }

    Ok(())
}

fn sha256(data: &[u8]) -> String {
    let mut hasher = Sha256::new();
    hasher.input(data);
    hasher.result().iter().map(|byte| format!("{:02x}", byte)).collect::<Vec<_>>().join("")
}
