use twmap::map_edit::calc_opaque_table;

use clap::{App, Arg};
use image::io::Reader as ImageReader;

use std::path::{Path, PathBuf};
use std::process::exit;

fn main() {
    let matches = App::new("twmap extract_tileflags")
        .about("Extracts the TILEFLAG_OPAQUE table for a image. TILEFLAG_OPAQUE is used for saving maps, opaque tiles in tiles layers are marked.")
        .arg(Arg::with_name("image")
            .help("Path to the image")
            .required(true)
            .index(1))
        .get_matches();

    let path = matches.value_of_os("image").unwrap();

    let table = match extract_table(&PathBuf::from(path)) {
        Err(err) => {
            println!("{}", err);
            exit(-1);
        }
        Ok(table) => table,
    };

    if table == [[false; 16]; 16] {
        println!("None of the tiles are opaque or the picture isn't a square");
        exit(1);
    }

    println!("[");
    for row in table.iter() {
        print!("    [");
        for i in 0..15 {
            print!("{:5}, ", row[i]);
        }
        println!("{:5}],", row[15]);
    }
    println!("]");
}

fn extract_table(path: &Path) -> Result<[[bool; 16]; 16], String> {
    let reader = ImageReader::open(path)
        .map_err(|err| err.to_string())?;
    let reader = reader.with_guessed_format()
        .map_err(|err| err.to_string())?;
    let image = reader.decode()
        .map_err(|err| err.to_string())?
        .to_rgba8();

    Ok(calc_opaque_table(&image))
}
