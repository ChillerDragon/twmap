use twmap::{TwMap, Layer};
use twmap::map_checks::envelope_kinds;
use twmap::datafile_parse::RawDatafile;

use clap::{App, Arg};

use std::fs;
use std::path::PathBuf;
use std::process::exit;

fn main() {
    let matches = App::new("twmap fix_map")
        .about("Loads broken binary teeworlds/ddnet maps, applies fixes configured through the arguments, tries to save them again.")
        .arg(Arg::with_name("INPUT-FILE")
            .help("Sets input map file path")
            .required(true)
            .index(1))
        .arg(Arg::with_name("OUTPUT-FILE")
            .help("Sets output file path")
            .required(true)
            .index(2))
        .arg(Arg::with_name("remove-faulty-quads")
            .long("remove-faulty-quads")
            .help("Goes through all quads and removes faulty ones"))
        .arg(Arg::with_name("zero-unused-tile-parts")
            .long("zero-unused-tile-parts")
            .help("Zero the parts of tiles that are unused and can't be edited with the common tools"))
        .arg(Arg::with_name("remove-all-envelopes")
            .long("remove-all-envelopes")
            .help("Remove all envelope items before parsing the map from the datafile"))
        .get_matches();

    let input_path = PathBuf::from(matches.value_of_os("INPUT-FILE").unwrap());
    let output_path = PathBuf::from(matches.value_of_os("OUTPUT-FILE").unwrap());

    let data = match fs::read(&input_path) {
        Ok(data) => data,
        Err(err) => {
            println!("io error: {}", err);
            exit(1);
        },
    };

    let mut datafile = match RawDatafile::parse(&data) {
        Ok(df) => df.to_datafile(),
        Err(err) => {
            println!("datafile parsing error: {}", err);
            exit(1);
        }
    };

    if matches.is_present("remove-all-envelopes") {
        // overwrite envelope items with an empty vector
        if let Some(removed_envelopes) = datafile.items.remove( &3) {
            println!("removed {} envelopes (and their points)", removed_envelopes.len());
        }
        else {
            println!("there were no envelopes to remove, envelope points will still be removed");
        }
        // overwrite envelope points with an empty vector
        if let Some(env_point_items) = datafile.items.get_mut(&6) {
            if let Some(env_point_item) = env_point_items.get_mut(0) {
                env_point_item.item_data = Vec::new();
            }
        }
    }

    let mut map = match TwMap::parse_datafile_unchecked(&datafile) {
        Err(err) => {
            println!("load error: {}", err);
            exit(1);
        },
        Ok(map) => map,
    };

    if let Err(err) = map.load_unchecked() {
        println!("error on decompression: {}", err);
        exit(1);
    }

    if matches.is_present("remove-faulty-quads") {
        remove_faulty_quads(&mut map);
    }

    if matches.is_present("zero-unused-tile-parts") {
        let zeroed_count = map.zero_unused_tile_parts();
        println!("zeroed {} different tile parts!", zeroed_count);
    }

    // (try to) save map
    if let Err(err) = map.save_file(&output_path) {
        println!("failed to save map: {}", err);
        exit(1);
    }
}

fn remove_faulty_quads(map: &mut TwMap) {
    let env_types = envelope_kinds(&map.envelopes);
    let mut count = 0;

    for group in &mut map.groups {
        for layer in &mut group.layers {
            if let Layer::Quads(quad_layer) = layer {
                let mut to_remove = Vec::new();

                for (i, quad) in quad_layer.quads.iter().enumerate() {
                    if let Err(err) = quad.check(&env_types) {
                        to_remove.push(i);
                        count += 1;
                        println!("\t{:?}\n\t\tThe {}th quad will be removed.\n\t\tQuad's position: {}", err, i + 1, quad.position);
                    }
                }

                for &i in to_remove.iter().rev() {
                    quad_layer.quads.remove(i);
                }
            }
        }
    }
    println!("Removed {} quad(s) in total.", count);
}
