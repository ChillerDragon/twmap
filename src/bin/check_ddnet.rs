use twmap::*;
use twmap::map_checks::MapError;

use clap::{App, Arg};

use std::collections::HashMap;
use std::ffi::OsString;
use std::fmt;

#[derive(Default)]
struct HookthroughAnalysis {
    faulty: u64,
    combo: u64,
    new: u64,
}

impl fmt::Display for HookthroughAnalysis {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{} faulty tiles were found.", self.faulty)?;
        writeln!(f, "{} combo tiles.", self.combo)?;
        writeln!(f, "{} new hookthrough tiles (excluding hookblock).", self.new)
    }
}

fn check_hookthrough(map: &mut TwMap, file_name: Option<&OsString>, results: &mut HookthroughAnalysis) -> Result<(), MapError> {
    if !map.find_physics_layer::<FrontLayer>().is_some() {
        return Ok(())
    }
    map.groups.load_conditionally(|layer| [LayerKind::Game, LayerKind::Front].contains(&layer.kind()))?;

    let game_tiles = map.find_physics_layer::<GameLayer>().unwrap()
        .tiles.unwrap_ref();
    let front_tiles = map.find_physics_layer::<FrontLayer>().unwrap()
        .tiles.unwrap_ref();

    results.faulty += game_tiles.indexed_iter()
        .zip(front_tiles.iter())
        .filter(|((_, game), front)|
            front.id == 5 && (game.id != 1 && game.id != 3))
        .map(|(((y, x), _), _)| {
            if let Some(path) = file_name {
                print!("{:?}: ", path);
            }
            println!("y={}, x={} -> wrongly placed old hookthrough", y, x);
        })
        .count() as u64;
    results.combo += game_tiles.iter()
        .zip(front_tiles.iter())
        .filter(|(game, front)|
            front.id == 5 && (game.id == 1 || game.id == 3))
        .count() as u64;
    results.new += game_tiles.iter()
        .zip(front_tiles.iter())
        .filter(|(game, front)|
            front.id == 66 && (game.id == 1 || game.id == 3))
        .count() as u64;
    Ok(())
}

fn check_direction_sensitive_tile_map<T: TileMapLayer>(layer: &T, results: &mut HashMap<u8, u64>, path: Option<&OsString>) -> Result<(), MapError> {
    layer.tiles().unwrap_ref().indexed_iter()
        .for_each(|((y, x), tile)| {
            let id = tile.id();
            let flags = tile.flags().unwrap();
            if is_sensitive(id) && (flags.contains(TileFlags::FLIP_H) ^ flags.contains(TileFlags::FLIP_V)) {
                if results.contains_key(&id) {
                    *results.get_mut(&id).unwrap() += 1
                }
                else {
                    results.insert(id, 1);
                }
                if let Some(path) = path {
                    print!("{:?}: ", path);
                };
                println!("y={}, x={} -> {} with invalid rotation", y, x, sensitive_id(id));
            }
        });
    Ok(())
}

fn check_direction_sensitive(map: &mut TwMap, path: Option<&OsString>, results: &mut HashMap<u8, u64>, ) -> Result<(), MapError> {
    map.groups.load_conditionally(|layer| [LayerKind::Game, LayerKind::Front, LayerKind::Switch].contains(&layer.kind()))?;
    check_direction_sensitive_tile_map(map.find_physics_layer::<GameLayer>().unwrap(), results, path)?;
    if let Some(front_layer) = map.find_physics_layer::<FrontLayer>() {
        check_direction_sensitive_tile_map(front_layer, results, path)?;
    }
    if let Some(switch_layer) = map.find_physics_layer::<SwitchLayer>() {
        check_direction_sensitive_tile_map(switch_layer, results, path)?;
    }
    Ok(())
}

fn is_sensitive(id: u8) -> bool {
    match id {
        60 | 61 | 64 | 65 | 67 | 203..=209 | 224 | 225 => true,
        _ => false,
    }
}

fn sensitive_id(id: u8) -> &'static str {
    match id {
        60 => "stopper",
        61 => "bi-directional stopper",
        64 => "arrow/speeder",
        65 => "double arrow/fast speeder",
        67 => "hookthrough",
        203..=209 => "spinning freezing laser",
        224 => "exploding bullet",
        225 => "freezing bullet",
        _ => unreachable!(),
    }
}

fn main() {
    let matches = App::new("twmap check_ddnet")
        .about("Scans ddnet maps for faulty behavior (e.g. falsely placed hook-through, invalid rotation of game tiles)")
        .arg(Arg::with_name("files")
            .required(true)
            .min_values(1)
            .index(1))
        .arg(Arg::with_name("hookthrough")
            .help("Check for faulty hookthrough: combo ht with no solid block underneath")
            .long("hookthrough"))
        .arg(Arg::with_name("directional-blocks")
            .help("Check for direction sensitive blocks in the physics layer which have an invalid orientation")
            .long("directional-blocks"))
        .arg(Arg::with_name("no-file-paths")
            .long("no-file-paths")
            .short("f")
            .help("Omit the file path at the start of each error info line"))
        .arg(Arg::with_name("no-summary")
            .long("no-summary")
            .help("Omit the short summary at the end"))
        .get_matches();
    let files = matches.values_of_os("files").unwrap()
        .map(|s| s.to_os_string())
        .collect::<Vec<OsString>>();

    let mut hookthrough_results = HookthroughAnalysis::default();
    let mut directional_sensitive_results = HashMap::<u8, u64>::default();

    for file_path in files.iter() {
        let mut map = match TwMap::parse_path(&file_path) {
            Err(err) => {
                eprintln!("[map loading error] {}", err);
                continue;
            }
            Ok(map) => map,
        };
        let printed_file_path = match matches.is_present("no-file-paths") {
            true => None,
            false => Some(file_path),
        };
        if matches.is_present("hookthrough") {
            if let Err(err) = check_hookthrough(&mut map, printed_file_path, &mut hookthrough_results) {
                eprintln!("{:?}: {}", &file_path, err);
            }
        }
        if matches.is_present("directional-blocks") {
            if let Err(err) = check_direction_sensitive(&mut map, printed_file_path, &mut directional_sensitive_results) {
                eprintln!("{:?}: {}", &file_path, err);
            }
        }
    }
    if !matches.is_present("no-summary") {
        if matches.is_present("hookthrough") {
            println!("{}", hookthrough_results);
        }
        if matches.is_present("directional-blocks") {
            for (id, num) in directional_sensitive_results {
                if num != 0 {
                    println!("{} (id {}) count: {}", sensitive_id(id), id, num);
                }
            }
        }
    }
}
