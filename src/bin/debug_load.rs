use twmap::{TwMap, Error, Version, LoadMultiple};
use twmap::map_parse::MapFromDatafileError;
use twmap::map_checks::MapError;
use twmap::map_dir::{MapDirError, MapDirErrorKind};
use twmap::datafile_parse::RawDatafile;

use clap::{App, Arg, ArgGroup};
use flexi_logger::{DeferredNow, Record, Logger, LogTarget};

use std::ffi::{OsString, OsStr};
use std::fmt;
use std::fs;
use std::io;
use std::path::Path;
use std::process::exit;

fn err<T: fmt::Display>(map_name: &OsString, err: T, verbose: bool) {
    if verbose {
        unsafe {
            match WRITE_FILE_PATH {
                true => println!("{:?}: {}", map_name, err),
                false => println!("{}", err),
            }
        }
    }
}

static mut CURRENT_FILE_PATH: Option<OsString> = None;
static mut WRITE_FILE_PATH: bool = false;

fn log_format(write: &mut dyn io::Write, _now: &mut DeferredNow, record: &Record) -> Result<(), io::Error> {
    unsafe {
        match WRITE_FILE_PATH {
            true => write!(write, "{:?}: [{}] {}", CURRENT_FILE_PATH.clone().unwrap(), record.level(), &record.args()),
            false => write!(write, "[{}] {}", record.level(), &record.args()),
        }
    }
}

const OTHER_INDEX: usize = 0;
const INFO_INDEX: usize = 1;
const IMAGE_INDEX: usize = 2;
const ENVELOPE_INDEX: usize = 3;
const GROUP_INDEX: usize = 4;
const LAYER_INDEX: usize = 5;
const SOUND_INDEX: usize = 6;
const VERSION_INDEX: usize = 7;
const DATAFILE_INDEX: usize = 8;

fn map_err_to_index(err: &MapError) -> usize {
    use MapError::*;
    match err {
        Info(_) => INFO_INDEX,
        Image(_) => IMAGE_INDEX,
        Envelope(_) => ENVELOPE_INDEX,
        Group(_) => GROUP_INDEX,
        Layer(_) => LAYER_INDEX,
        Sound(_) => SOUND_INDEX,
        Vanilla(_) | DDNet(_) => VERSION_INDEX,
    }
}

fn map_from_df_err_to_index(err: &MapFromDatafileError) -> usize {
    use MapFromDatafileError::*;
    match err {
        Datafile(_) | UuidIndex(_) | MapVersion(_) => OTHER_INDEX,
        Info(_) => INFO_INDEX,
        Image(_) => IMAGE_INDEX,
        Envelope(_) => ENVELOPE_INDEX,
        EnvPoint(_) => ENVELOPE_INDEX,
        Group(_) => GROUP_INDEX,
        Layer(_) => LAYER_INDEX,
        Sound(_) => SOUND_INDEX,
        AutoMapper(_) => LAYER_INDEX,
    }
}

fn path_err_to_index(path: &Path) -> usize {
    let components: Vec<&OsStr> = path.components()
        .map(|comp| comp.as_os_str())
        .collect();
    if components.contains(&OsStr::new("images")) {
        IMAGE_INDEX
    }
    else if components.contains(&OsStr::new("envelopes")) {
        ENVELOPE_INDEX
    }
    else if components.contains(&OsStr::new("groups")) {
        if components.contains(&OsStr::new("layers")) {
            LAYER_INDEX
        }
        else {
            GROUP_INDEX
        }
    }
    else if components.contains(&OsStr::new("sounds")) {
        SOUND_INDEX
    }
    else if components.contains(&OsStr::new("info")) {
        INFO_INDEX
    }
    else {
        OTHER_INDEX
    }
}

fn dir_err_to_index(err: &MapDirError) -> usize {
    use MapDirErrorKind::*;
    match err.kind {
        InvalidUtf8 => path_err_to_index(&err.path),
        PhysicsLayerName(_) => LAYER_INDEX,
        ColorDepth16 => IMAGE_INDEX,
        DuplicateImageName => IMAGE_INDEX,
        Io(_) => path_err_to_index(&err.path),
        Json(_) => path_err_to_index(&err.path),
        Image(_) => IMAGE_INDEX,
    }
}

fn from_index(index: usize) -> &'static str {
    match index {
        OTHER_INDEX => "Other",
        INFO_INDEX => "Info",
        IMAGE_INDEX => "Image",
        ENVELOPE_INDEX => "Envelope",
        GROUP_INDEX => "Group",
        LAYER_INDEX => "Layer",
        SOUND_INDEX => "Sound",
        VERSION_INDEX => "Version",
        _ => unreachable!(),
    }
}

fn main() {
    let matches = App::new("twmap debug_load")
        .about("Tries to parse various parts of teeworlds map files according to the passed arguments")
        .arg(Arg::with_name("files")
            .required(true)
            .min_values(1)
            .index(1))
        .arg(Arg::with_name("verbose")
            .short("v")
            .multiple(true)
            .help("Level of verbosity: -v => errors, -vv => also warn logs, -vvv => also info logs"))
        .arg(Arg::with_name("datafile-only")
            .help("Only parse the datafile, not the map")
            .long("datafile-only"))
        .arg(Arg::with_name("no-checks")
            .help("Only parse the TwMap struct, omit further checks")
            .long("no-checks"))
        .arg(Arg::with_name("keep-compressed")
            .long("keep-compressed")
            .help("Don't decompress loadable data items. If this is not set, they will be loaded if no other errors were found"))
        .group(ArgGroup::with_name("parsing-options")
            .args(&["datafile-only", "no-checks", "keep-compressed"]))
        .arg(Arg::with_name("no-summary")
            .long("no-summary")
            .help("Omit the short summary of encountered errors at the end"))
        .arg(Arg::with_name("full-summary")
            .long("full-summary")
            .help("Don't emit the summary entries with 0 findings"))
        .group(ArgGroup::with_name("summary-options")
            .args(&["no-summary", "full-summary"]))
        .arg(Arg::with_name("no-file-paths")
            .long("no-file-paths")
            .short("f")
            .help("Omit the file path at the start of each error info line"))
        .get_matches();

    let files = matches.values_of_os("files").unwrap()
        .map(|s| s.to_os_string())
        .collect::<Vec<OsString>>();

    unsafe {
        WRITE_FILE_PATH = !matches.is_present("no-file-paths");
    }

    let verbosity = matches.occurrences_of("verbose");
    let verbose = match verbosity {
        0 => false,
        _ => true,
    };
    if verbosity == 2 {
        Logger::with_str("warn")
            .log_target(LogTarget::StdOut)
            .format(log_format)
            .start().unwrap();
    }
    else if verbosity >= 3 {
        Logger::with_str("info")
            .log_target(LogTarget::StdOut)
            .format(log_format)
            .start().unwrap();
    }

    let mut counters = [0; 9];
    let mut ddnet_count = 0;
    let mut teeworlds_07_count = 0;
    let mut datafile_count = 0; // used only for datafile-only

    for file_path in files.iter() {
        unsafe {
            CURRENT_FILE_PATH = Some(file_path.clone());
        }
        if matches.is_present("datafile-only") {
            let data = match fs::read(&file_path) {
                Ok(data) => data,
                Err(error) => {
                    println!("io error on path {:?}: {}", file_path, error);
                    exit(1);
                }
            };
            if let Err(error) = RawDatafile::parse(&data)
                .map(|df| df.to_datafile()) {
                counters[DATAFILE_INDEX] += 1;
                err(file_path, error, verbose);
            }
            else {
                datafile_count += 1;
            }
        }
        else {
            match TwMap::parse_path_unchecked(file_path) {
                Ok(mut map) => {
                    match map.version {
                        Version::Teeworlds07 => teeworlds_07_count += 1,
                        Version::DDNet06 => ddnet_count += 1,
                    }
                    if !matches.is_present("no-checks") {
                        let errors = map.check_individually();
                        for error in &errors {
                            counters[map_err_to_index(error)] += 1;
                            err(file_path, error, verbose);
                        }
                        if errors.is_empty() && !matches.is_present("keep-compressed") {
                            if let Err(error) = map.images.load() {
                                counters[IMAGE_INDEX] += 1;
                                err(file_path, error, verbose)
                            }
                            if let Err(error) = map.groups.load() {
                                counters[LAYER_INDEX] += 1;
                                err(file_path, error, verbose)
                            }
                            if let Err(error) = map.sounds.load() {
                                counters[SOUND_INDEX] += 1;
                                err(file_path, error, verbose)
                            }
                        }
                    }
                },
                Err(Error::Io(error)) => {
                    println!("io error on path {:?}: {}", file_path, error);
                    exit(1);
                }
                Err(Error::DatafileParse(error)) => {
                    counters[DATAFILE_INDEX] += 1;
                    err(file_path, error, verbose);
                },
                Err(Error::MapFromDatafile(error)) => {
                    counters[map_from_df_err_to_index(&error)] += 1;
                    err(file_path, error, verbose);
                },
                Err(Error::Dir(error)) => {
                    counters[dir_err_to_index(&error)] += 1;
                    err(file_path, error, verbose);
                }
                _ => unreachable!()
            }
        }
    }

    if !matches.is_present("no-summary") {
        if matches.is_present("datafile-only") {
            println!("Successfully loaded {} datafiles", datafile_count);
        }
        else {
            println!("Successfully loaded {} ddnet/0.6 maps and {} teeworlds 0.7 maps", ddnet_count, teeworlds_07_count);
            if counters[DATAFILE_INDEX] != 0 {
                println!("{} of the given files could not be parsed as maps", counters[DATAFILE_INDEX]);
            }
        }
        for i in 0..counters.len() - 1 {
            if counters[i] != 0 || matches.is_present("full-summary") {
                println!("{} errors: {}", from_index(i), counters[i]);
            }
        }
    }
}
