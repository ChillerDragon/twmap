use crate::map::*;
use crate::map_checks::{MapError, CheckData, ImageErrorKind, LayerErrorKind, SoundErrorKind, CompressedLayerError, ImageError, LayerError, SoundError};
use crate::compression::{decompress, DecompressionError};
use crate::convert::{To, TryTo};

use image::RgbaImage;
use ndarray::Array2;

use std::{iter, mem};

pub trait TileParsing: AnyTile {
    // wrapper function for parse_tiles_raw to allow decompression for the tile type 'Tile' to be put in between the two steps
    fn parse_tiles(data: Vec<u8>, width: usize, height: usize, _compression: bool) -> Result<Array2<Self>, DecompressionError> {
        Ok(Self::parse_tiles_raw(data, width, height))
    }

    // generalized parsing
    fn parse_tiles_raw(data: Vec<u8>, width: usize, height: usize) -> Array2<Self> {
        let tiles = match Self::view_boxed_slice(data.into_boxed_slice()) {
            Ok(tiles) => tiles.into_vec(),
            Err(_) => panic!("Viewing of data vector failed."),
        };
        match Array2::from_shape_vec((height, width), tiles) {
            Ok(tiles) => tiles,
            Err(_) => panic!("Error while converting vector to 2d-array."),
        }
    }
}

impl TileParsing for Tile {
    // the length of data is guaranteed to be divisible by 4 by check_uncompressed
    fn parse_tiles(mut data: Vec<u8> , width: usize, height: usize, compression: bool) -> Result<Array2<Self>, DecompressionError> {
        if compression {
            assert_eq!(data.len() % 4, 0);
            data = data.chunks(4)
                .flat_map(|tile | {
                    iter::repeat(tile).take(tile[2].to::<usize>() + 1)
                        .flat_map(|tile| [tile[0], tile[1], 0, tile[3]].to_vec())
                })
                .collect()
        }
        let expected_size = width * height * mem::size_of::<Tile>();
        if data.len() > expected_size {
            Err(DecompressionError::TooBig)
        }
        else if data.len() < expected_size {
            Err(DecompressionError::TooSmall)
        }
        else {
            Ok(Tile::parse_tiles_raw(data, width, height))
        }
    }
}

impl TileParsing for GameTile {
    // the length of data is guaranteed to be divisible by 4 by check_uncompressed
    fn parse_tiles(mut data: Vec<u8> , width: usize, height: usize, compression: bool) -> Result<Array2<Self>, DecompressionError> {
        if compression {
            data = data.chunks(4)
                .flat_map(|tile | {
                    iter::repeat(tile).take(tile[2].to::<usize>() + 1)
                        .flat_map(|tile| [tile[0], tile[1], 0, tile[3]].to_vec())
                })
                .collect()
        }
        let expected_size = width * height * mem::size_of::<Tile>();
        if data.len() > expected_size {
            Err(DecompressionError::TooBig)
        }
        else if data.len() < expected_size {
            Err(DecompressionError::TooSmall)
        }
        else {
            Ok(GameTile::parse_tiles_raw(data, width, height))
        }
    }
}

impl TileParsing for Tele {}

impl TileParsing for Switch {}

impl TileParsing for Speedup {}

impl TileParsing for Tune {}

pub trait Load: CheckData + Sized {
    fn load_unchecked(&mut self) -> Result<(), Self::CompressedErrorKind>;

    fn load(&mut self) -> Result<(), Self::ErrorKind> {
        self.load_unchecked()?;
        self.check_loaded_data()?;
        Ok(())
    }
}

impl Load for CompressedData<Vec<u8>, ()> {
    fn load_unchecked(&mut self) -> Result<(), Self::CompressedErrorKind> {
        self.check_compressed_data()?;
        if let CompressedData::Compressed(compressed_data, size, _) = self {
            let decompressed_data = decompress(compressed_data, *size)?;
            *self = CompressedData::Loaded(decompressed_data);
        }
        Ok(())
    }
}

impl Load for CompressedData<RgbaImage, ImageLoadInfo> {
    fn load_unchecked(&mut self) -> Result<(), Self::CompressedErrorKind> {
        self.check_compressed_data()?;
        if let CompressedData::Compressed(compressed_data, size, info) = self {
                let decompressed_data = decompress(compressed_data, *size)?;
                let loaded_data = <RgbaImage>::from_raw(info.width.try_to(), info.height.try_to(), decompressed_data).unwrap();
                *self = CompressedData::Loaded(loaded_data);
        }
        Ok(())
    }
}

impl<T: TileParsing> Load for CompressedData<Array2<T>, TilesLoadInfo>
    where CompressedData<Array2<T>, TilesLoadInfo>: CheckData<CompressedErrorKind=CompressedLayerError, ErrorKind=LayerErrorKind> {
    fn load_unchecked(&mut self) -> Result<(), CompressedLayerError> {
        self.check_compressed_data()?;
        if let CompressedData::Compressed(compressed_data, size, info) = self {
            let decompressed_data = decompress(compressed_data, *size)
                .map_err(|err| CompressedLayerError::ZLibDecompression(err))?;
            let tiles = T::parse_tiles(decompressed_data, info.width.try_to(), info.height.try_to(), info.compression)
                .map_err(|err| CompressedLayerError::VanillaDecompression(err))?;
            *self = CompressedData::Loaded(tiles);
        }
        Ok(())
    }
}

pub trait LoadMultiple {
    type ErrorType;

    type ItemType;

    fn load_unchecked(&mut self) -> Result<(), Self::ErrorType>;

    fn load_conditionally(&mut self, condition: impl Fn(&Self::ItemType) -> bool + Copy) -> Result<(), Self::ErrorType>;

    fn load(&mut self) -> Result<(), Self::ErrorType> {
        self.load_conditionally(|_| true)
    }
}

impl LoadMultiple for [Image] {
    type ErrorType = ImageError;
    type ItemType = Image;

    fn load_unchecked(&mut self) -> Result<(), Self::ErrorType> {
        for (i, image) in self.iter_mut().enumerate() {
            if let Image::Embedded(image) = image {
                image.image.load_unchecked()
                    .map_err(|err| ImageError {
                        index: Some(i),
                        kind: ImageErrorKind::from(err),
                    })?;
            }
        }
        Ok(())
    }

    fn load_conditionally(&mut self, condition: impl Fn(&Self::ItemType) -> bool) -> Result<(), Self::ErrorType> {
        for (i, image) in self.iter_mut().enumerate() {
            if condition(image) {
                if let Image::Embedded(image) = image {
                    image.image.load()
                        .map_err(|kind| ImageError {
                            index: Some(i),
                            kind,
                        })?;
                }
            }
        }
        Ok(())
    }
}

impl LoadMultiple for [Layer] {
    type ErrorType = LayerError;
    type ItemType = Layer;

    fn load_unchecked(&mut self) -> Result<(), Self::ErrorType> {
        for (i, layer) in self.iter_mut().enumerate() {
            use Layer::*;
            match layer {
                Game(l) => l.tiles.load_unchecked(),
                Tiles(l) => l.tiles.load_unchecked(),
                Front(l) => l.tiles.load_unchecked(),
                Tele(l) => l.tiles.load_unchecked(),
                Speedup(l) => l.tiles.load_unchecked(),
                Switch(l) => l.tiles.load_unchecked(),
                Tune(l) => l.tiles.load_unchecked(),
                Quads(_) | Sounds(_) | Invalid(_) => Ok(())
            }.map_err(|err| LayerError {
                layer_group: Some((i, None)),
                layer_kind: Some(layer.kind()),
                kind: LayerErrorKind::from(err),
            })?;
        }
        Ok(())
    }

    fn load_conditionally(&mut self, condition: impl Fn(&Self::ItemType) -> bool) -> Result<(), Self::ErrorType> {
        for (i, layer) in self.iter_mut().enumerate() {
            if condition(layer) {
                use Layer::*;
                match layer {
                    Game(l) => l.tiles.load(),
                    Tiles(l) => l.tiles.load(),
                    Front(l) => l.tiles.load(),
                    Tele(l) => l.tiles.load(),
                    Speedup(l) => l.tiles.load(),
                    Switch(l) => l.tiles.load(),
                    Tune(l) => l.tiles.load(),
                    Quads(_) | Sounds(_) | Invalid(_) => Ok(())
                }.map_err(|kind| LayerError {
                    layer_group: Some((i, None)),
                    layer_kind: Some(layer.kind()),
                    kind,
                })?;
            }
        }
        Ok(())
    }
}

impl LoadMultiple for [Group] {
    type ErrorType = LayerError;
    type ItemType = Layer;

    fn load_unchecked(&mut self) -> Result<(), Self::ErrorType> {
        for (i, group) in self.iter_mut().enumerate() {
            group.layers.load_unchecked()
                .map_err(|mut err| {
                    err.layer_group = Some((err.layer_group.unwrap().0, Some(i)));
                    err
                })?;
        }
        Ok(())
    }

    fn load_conditionally(&mut self, condition: impl Fn(&Self::ItemType) -> bool + Copy) -> Result<(), Self::ErrorType> {
        for (i, group) in self.iter_mut().enumerate() {
            group.layers.load_conditionally(condition)
                .map_err(|mut err| {
                    err.layer_group = Some((err.layer_group.unwrap().0, Some(i)));
                    err
                })?;
        }
        Ok(())
    }
}

impl LoadMultiple for [Sound] {
    type ErrorType = SoundError;
    type ItemType = Sound;

    fn load_unchecked(&mut self) -> Result<(), Self::ErrorType> {
        for (i, sound) in self.iter_mut().enumerate() {
            sound.data.load_unchecked()
                .map_err(|err| SoundError {
                    index: Some(i),
                    kind: SoundErrorKind::from(err),
                })?;
        }
        Ok(())
    }

    fn load_conditionally(&mut self, condition: impl Fn(&Self::ItemType) -> bool) -> Result<(), Self::ErrorType> {
        for (i, sound) in self.iter_mut().enumerate() {
            if condition(sound) {
                sound.data.load_unchecked()
                    .map_err(|err| SoundError {
                        index: Some(i),
                        kind: SoundErrorKind::from(err),
                    })?;
            }
        }
        Ok(())
    }
}

impl TwMap {
    pub fn load(&mut self) -> Result<(), MapError> {
        self.images.load()?;
        self.groups.load()?;
        self.sounds.load()?;
        Ok(())
    }

    pub fn load_unchecked(&mut self) -> Result<(), MapError> {
        self.images.load_unchecked()?;
        self.groups.load_unchecked()?;
        self.sounds.load_unchecked()?;
        Ok(())
    }
}
