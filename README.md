TwMap
===

Safely parse, edit and save [Teeworlds](https://www.teeworlds.com/) and [DDNet](https://ddnet.tw/) maps.
Goals of this library:

- performance
- reasonable map standards
- compatibility with all Teeworlds [maps](https://github.com/teeworlds/teeworlds-maps) (0.7) and DDNet (0.6) [maps](https://github.com/ddnet/ddnet-maps)

The binary map format is documented in the libtw2 repo [here](https://github.com/heinrich5991/libtw2/blob/master/doc/map.md).

Supported map formats
---

- DDNet (0.6)
- Vanilla (0.7)
- MapDir
    - folder/directory based format
    - embedded images as .png files
    - embedded sounds as .opus files
    - all other parts of the struct as .json files

Building
---

You need [Rust](https://www.rust-lang.org/tools/install) installed on your system.
To compile twmap in release mode, execute the following command in the source root:
```
cargo build --release
```
The executable files will be located in `target/release/`.

To compile the debug build instead, omit the `--release` flag.
The executable files of the debug will be located in `target/debug/`.

Compression
---

Parts of the binary map data are compressed with zlib compression, this is the main slowdown factor when loading maps.
To compensate for this, the library will not load layer, image and sound data on parsing, instead this can be done manually.

If you want to save the map again, then the library will decompress everything anyways (its included in the `save` method).
However, if that is not the case, and you only want to read some part of it, you can hold off on performing the `load` method on the map struct.
Instead, perform `load` only on the compressed parts of the map that you want to read.

Note that some methods provided by the library require parts of it to be loaded properly. Keep that in mind while playing around with partially unloaded maps.

Checks
---

This library has a lot of constraints on its map struct that it enforces on saving maps.
For instance every map must have a Game group, Game layer, layer names must be at most 11 bytes, etc.
All constraints are put into place to ensure proper saving and parsing, meaning that the library guarantees you well-behaved maps.

Note that many methods provided by the library require certain constraints to be fulfilled.
Keep that in mind when using `parse_unchecked` instead of `parse`, which will leave out most of the checks.

Executables
---

All executables are command line argument based and explain their use with `--help`.

- `debug_load`: parse maps with different verbosity levels for debugging purposes
- `edit_map`: easily convert maps between different formats, with some fancy steps in between like mirroring
- `fix_map`: fix some common issues on maps which are not accepted by other executables
- `extract_files`: extract image and sound files from maps
- `extract_tileflags`: used on images, extracts the `TILEFLAG_OPAQUE` table used in tiles layers
- `check_ddnet`: scans ddnet maps for faulty behavior (e.g. falsely placed hook-through, invalid rotation of game tiles)

Credits
---

Thanks to ChillerDragon for sponsoring the creation of the MapDir map format.
