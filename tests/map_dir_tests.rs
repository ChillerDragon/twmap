use twmap::*;

fn save_load_eq(mut map: TwMap) {
    let mut bin_data = Vec::new();
    map.save(&mut bin_data).unwrap();
    let mut bin_loaded_map = TwMap::parse(&bin_data).unwrap();
    bin_loaded_map.load().unwrap();

    let tmp_dir = tempfile::tempdir().unwrap();
    let path = tmp_dir.path().join("map");
    map.save_dir(&path).unwrap();
    let dir_loaded_map = TwMap::parse_dir(&path).unwrap();

    assert_eq!(dir_loaded_map, map);
}

#[test]
fn dm1_dir() {
    let dm1 = TwMap::parse_file("tests/dm1.map").unwrap();
    save_load_eq(dm1);
}

#[test]
fn editor_test() {
    let editor_map = TwMap::parse_file("tests/editor.map").unwrap();
    save_load_eq(editor_map);
}
