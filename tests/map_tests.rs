use twmap::*;

use image::RgbaImage;
use ndarray::arr2;

use std::fs::File;
use std::io::Read;

fn map_load_save_load_save_eq(path: &str, version: Version) {
    let mut map_data = Vec::new();
    let mut map_file = File::open(path).unwrap();
    map_file.read_to_end(&mut map_data).unwrap();
    let mut map = TwMap::parse(&map_data).unwrap();
    assert_eq!(map.version, version);

    map_save_load_save_eq(&mut map);
}

fn map_save_load_save_eq(original_map: &mut TwMap) {
    original_map.load().unwrap();

    let mut first_saved_data = Vec::new();
    original_map.save(&mut first_saved_data).unwrap();
    let mut loaded_map = TwMap::parse(&first_saved_data).unwrap();
    loaded_map.load().unwrap();
    assert_eq!(loaded_map, *original_map);

    let mut second_saved_data = Vec::new();
    loaded_map.save(&mut second_saved_data).unwrap();
    assert_eq!(second_saved_data, first_saved_data);
}

fn generate_data(count: usize, step: u8, reverse: bool) -> Vec<u8> {
    let mut data = Vec::new();
    let mut value: u8 = 0;
    for _ in 0..count {
        data.push(value);
        value = value.wrapping_add(step);
    }
    if reverse {
        data.reverse();
    }
    data
}

#[test]
fn dm1_test_vanilla() {
    map_load_save_load_save_eq("tests/dm1.map", Version::Teeworlds07);
}

#[test]
fn editor_test() {
    map_load_save_load_save_eq("tests/editor.map", Version::DDNet06);
}

#[test]
fn tile_flags_ddnet_test() {
    let mut original_map = TwMap::parse_file("tests/tileflags_ddnet.map").unwrap();
    assert_eq!(original_map.version, Version::DDNet06);
    original_map.load().unwrap();
    let mut own_map = original_map.clone();

    own_map.process_tile_flag_opaque();
    assert_eq!(own_map, original_map);
    own_map.version = Version::Teeworlds07;
    own_map.process_tile_flag_opaque();
    assert_ne!(own_map, original_map);
}

#[test]
fn custom_test_ddnet() {
    let tf: Vec<TileFlags> = (0..16).into_iter()
        .map(|n| TileFlags::from_bits(n).unwrap())
        .collect();

    let mut map = TwMap {
        version: Version::DDNet06,
        info: Info {
            author: String::from("Mapper with name of any length"),
            version: String::new(),
            credits: String::from(""),
            license: String::new(),
            settings: vec![String::from("Setting 1"), String::from("Unnecessary long setting abcdefghijklmnopqrstuvwxyz"), String::from("Setting 3")]
        },
        images: vec![
            Image::Embedded(EmbeddedImage {
                name: "name_with_128_bytes_length_restriction_image_1".to_string(),
                image: RgbaImage::from_raw(16, 32, generate_data(32 * 16 * 4, 1, false)).unwrap().into(),
            }),
            Image::External(ExternalImage {
                width: 1337,
                height: 42,
                name: "sun".to_string(),
            }),
            Image::Embedded(EmbeddedImage {
                name: "".to_string(),
                image: RgbaImage::from_raw(64, 32, generate_data(64 * 32 * 4, 2, true)).unwrap().into(),
            }),
        ],
        envelopes: vec![
            Envelope::Sound(Env {
                points: vec![
                    EnvPoint {
                        time: 0,
                        curve: CurveKind::Step,
                        content: 1
                    },
                    EnvPoint {
                        time: 1,
                        curve: CurveKind::Fast,
                        content: 2
                    }
                ],
                name: "abcdefghijklmnopqrstuvwxyzabcde".to_string(),
                synchronized: false
            }),
            Envelope::Sound(Env {
                points: vec![
                    EnvPoint {
                        time: 0,
                        curve: CurveKind::Linear,
                        content: 3
                    },
                    EnvPoint {
                        time: i32::MAX,
                        curve: CurveKind::Slow,
                        content: i32::MAX
                    }
                ],
                name: "".to_string(),
                synchronized: true
            }),
            Envelope::Position(Env {
                points: vec![
                    EnvPoint {
                        time: 0,
                        curve: CurveKind::Step,
                        content: Position {
                            x: 1,
                            y: 2,
                            rotation: 0,
                        },
                    },
                    EnvPoint {
                        time: i32::MAX,
                        curve: CurveKind::Fast,
                        content: Position {
                            x: i32::MAX,
                            y: i32::MAX,
                            rotation: i32::MAX,
                        },
                    }
                ],
                name: "abcdefghijklmnopqrstuvwxyzabcde".to_string(),
                synchronized: false
            }),
            Envelope::Color(Env {
                points: vec![
                    EnvPoint {
                        time: 0,
                        curve: CurveKind::Smooth,
                        content: I32Color { r: 1, g: 2, b: 3, a: 4 }
                    },
                    EnvPoint {
                        time: i32::MAX,
                        curve: CurveKind::Linear,
                        content: I32Color { r: i32::MAX, g: i32::MAX, b: i32::MAX, a: i32::MAX }
                    }
                ],
                name: "abcdefghijklmnopqrstuvwxyzabcde".to_string(),
                synchronized: false
            }),
        ],
        groups: vec![
            Group {
                offset_x: 0,
                offset_y: 0,
                parallax_x: 100,
                parallax_y: 100,
                layers: vec![
                    Layer::Game(GameLayer {
                        tiles: CompressedData::Loaded(arr2(&[
                            [GameTile::new(1, tf[0]), GameTile::new(2, tf[1]), GameTile::new(3, tf[2])],
                            [GameTile::new(4, tf[3]), GameTile::new(5, tf[8]), GameTile::new(6, tf[9])],
                        ]))
                    }),
                    Layer::Tiles(TilesLayer {
                        detail: true,
                        color: Color { r: 1, g: 2, b: 3, a: 4 },
                        color_env: Some(3),
                        color_env_offset: 21,
                        image: Some(2),
                        tiles: CompressedData::Loaded(arr2(&[
                            [Tile::new(1, tf[6]), Tile::new(2, tf[7]) , Tile::new(3, tf[8]) ],
                            [Tile::new(4, tf[9]), Tile::new(5, tf[10]), Tile::new(6, tf[11])],
                        ])),
                        name: "abcdefghijk".to_string(),
                        auto_mapper: AutoMapper {
                            config: None,
                            seed: 21,
                            automatic: false
                        },
                    })],
                clipping: false,
                clip_x: 0,
                clip_y: 0,
                clip_width: 0,
                clip_height: 0,
                name: "Game".to_string()
            },
            Group {
                offset_x: 1,
                offset_y: 2,
                parallax_x: 3,
                parallax_y: 4,
                layers: vec![
                    Layer::Sounds(SoundsLayer {
                        detail: false,
                        sources: vec![
                            SoundSource {
                                position: Point { x: 21, y: 42},
                                looping: true,
                                panning: false,
                                delay: 21,
                                falloff: 123,
                                position_env: Some(2),
                                position_env_offset: 3,
                                sound_env: Some(0),
                                sound_env_offset: 7,
                                shape: SoundShape::Circle { radius: 37 }
                            },
                            SoundSource {
                                position: Point { x: 43, y: 68},
                                looping: false,
                                panning: true,
                                delay: 42,
                                falloff: 246,
                                position_env: None,
                                position_env_offset: 0,
                                sound_env: Some(1),
                                sound_env_offset: 14,
                                shape: SoundShape::Rectangle { width: 12, height: 34 }
                            }
                        ],
                        sound: None,
                        name: "".to_string()
                    }),
                    Layer::Quads(QuadsLayer {
                                             detail: true,
                                             quads: vec![
                                                 Quad {
                                                     position: Point { x: 12, y: 24 },
                                                     corners: [Point { x: 1, y: 2 }, Point { x: 3, y: 4 }, Point { x: 5, y: 6 }, Point { x: 7, y: 8 }],
                                                     colors: [Color { r: 9, g: 10, b: 11, a: 12 }, Color { r: 13, g: 14, b: 15, a: 16 }, Color { r: 17, g: 18, b: 19, a: 20 }, Color { r: 21, g: 22, b: 23, a: 24 }],
                                                     texture_coords: [Point { x: 25, y: 26 }, Point { x: 27, y: 28 }, Point { x: 29, y: 30 }, Point { x: 31, y: 32 }],
                                                     position_env: Some(2),
                                                     position_env_offset: 33,
                                                     color_env: Some(3),
                                                     color_env_offset: 34
                                                 }
                                             ],
                                             image: Some(1),
                                             name: "another3i32".to_string()
                                         }),
                    Layer::Tiles(TilesLayer {
                                             detail: true,
                                             color: Color { r: 21, g: 42, b: 63, a: 84 },
                                             color_env: Some(3),
                                             color_env_offset: 13,
                                             image: Some(0),
                                             tiles: CompressedData::Loaded(arr2(&[
                                                 [Tile::new(11, tf[12]), Tile::new(13, tf[13]), Tile::new(15, tf[14])],
                                                 [Tile::new(17, tf[15]), Tile::new(19, tf[14]), Tile::new(21, tf[13])],
                                             ])),
                                             name: "ABCDEFGHIJK".to_string(),
                                             auto_mapper: AutoMapper {
                                                 config: Some(1),
                                                 seed: 2121,
                                                 automatic: true
                                             }
                                         })
                ],
                clipping: true,
                clip_x: 5,
                clip_y: 6,
                clip_width: 7,
                clip_height: 8,
                name: "3intsstring".to_string()
            }
        ],
        sounds: vec![],
    };
    let mut data = Vec::new();
    map_save_load_save_eq(&mut map);
    map.version = Version::Teeworlds07;
    assert!(map.save(&mut data).is_err());
}

#[test]
fn custom_test_teeworlds07() {
    let tf: Vec<TileFlags> = (0..16).into_iter()
        .map(|n| TileFlags::from_bits(n).unwrap())
        .collect();

    let mut map = TwMap {
        version: Version::Teeworlds07,
        info: Default::default(),
        images: vec![],
        envelopes: vec![
            Envelope::Color(Env {
                name: "Vanilla envelope".to_string(),
                synchronized: false,
                points: vec![
                    EnvPoint {
                        time: 0,
                        curve: CurveKind::Bezier(BezierCurve {
                            in_tangent_dx: I32Color {
                                r: 0,
                                g: 1,
                                b: 2,
                                a: 3
                            },
                            in_tangent_dy: I32Color {
                                r: 4,
                                g: 5,
                                b: 6,
                                a: 7,
                            },
                            out_tangent_dx: I32Color {
                                r: 8,
                                g: 9,
                                b: 10,
                                a: 11
                            },
                            out_tangent_dy: I32Color {
                                r: 12,
                                g: 13,
                                b: 14,
                                a: 15
                            }
                        }),
                        content: I32Color {
                            r: 0,
                            g: 7,
                            b: 14,
                            a: 21,
                        }
                    }],
            }),
            Envelope::Position(Env {
                name: "".to_string(),
                synchronized: true,
                points: vec![
                    EnvPoint {
                        time: 1,
                        content: Position {
                            x: 123,
                            y: 456,
                            rotation: 789,
                        },
                        curve: CurveKind::Bezier(BezierCurve {
                            in_tangent_dx: Position {
                                x: 16,
                                y: 17,
                                rotation: 18,
                            },
                            in_tangent_dy: Position {
                                x: 19,
                                y: 20,
                                rotation: 21,
                            },
                            out_tangent_dx: Position {
                                x: 22,
                                y: 23,
                                rotation: 24,
                            },
                            out_tangent_dy: Position {
                                x: 25,
                                y: 26,
                                rotation: 27,
                            }
                        })
                    }
                ]
            })
        ],
        groups: vec![Group {
            offset_x: 0,
            offset_y: 0,
            parallax_x: 100,
            parallax_y: 100,
            layers: vec![
                Layer::Game(GameLayer {
                    tiles: CompressedData::Loaded(arr2(&[
                        [GameTile::new(1, tf[0]), GameTile::new(2, tf[1]), GameTile::new(3, tf[2])],
                        [GameTile::new(4, tf[3]), GameTile::new(5, tf[8]), GameTile::new(6, tf[9])],
                    ]))
                    })],
            clipping: false,
            clip_x: 0,
            clip_y: 0,
            clip_width: 0,
            clip_height: 0,
            name: "Game".to_string()
        }],
        sounds: vec![],
    };

    let mut data = Vec::new();
    map_save_load_save_eq(&mut map);
    map.version = Version::DDNet06;
    assert!(map.save(&mut data).is_err());
}
